# Chapters

This repository holds the chapters of the **grade project chapters**, in this README you will find the TODO list of all chapters

## Proposal

### TODO: Include in chapters

- [x] Problem
- [x] Proposed Solution
- [x] Objectives
- [x] Reach
- [x] Justification

## Literature

### TODO: Include in chapters

#### References

- [ ] ASP .NET Core
- [ ] SWR
- [ ] .NET Implementation
- [ ] HAProxy

#### Basis

- [x] Microservices architecture
- [x] Web Service
- [ ] Concurrency (Cooperative)
- [ ] Idempotency
- [ ] Memory Mapped Files
- [ ] Memoization
- [x] Plugin architecture
- [x] Proxy Reverso

#### Techniques

- [ ] Profiling
- [ ] Load testing
- [ ] Benchmarking
- [ ] RPC

#### Technologies

- [x] CLI
- [ ] WebAssembly
- [ ] Wasmtime
- [x] k6
- [ ] Tokio
- [ ] fmmap
- [ ] bincode
- [ ] flamegraph-rs
- [ ] waPC
- [ ] hyper
- [ ] Rust
- [ ] Golang
- [ ] tinygo
- [ ] Flatbuffers

### Metrics

- [x] Latency
- [ ] Requests per second
- [ ] Response time

## Methodology

### TODO: Include in chapters

- [x] Sprint 1
- [x] Sprint 2
- [x] Sprint 3
- [ ] Sprint 4
- [ ] Sprint 5
- [ ] Sprint 6
- [ ] Sprint 7
- [ ] Sprint 8
- [ ] Sprint 9
- [ ] Sprint 10

## Results

### TODO: Include in chapters

- [ ] Sprint 3
- [ ] Sprint 4
- [ ] Sprint 5
- [ ] Sprint 6
- [ ] Sprint 7
- [ ] Sprint 8
- [ ] Sprint 9
- [ ] Sprint 10

## Conclusions

### TODO: Include in chapters

- [ ] Findings
- [ ] Recommendations
