## Iteracion 4

- Performance tunning del sprint anterior

  - Se decide implementar una segunda version del _proxy reverso_, llamada _Direct Handler_ la cual se diferencia
    de la implementacion previa, _Routed Handler_, de la siguiente manera:
      - El _Routed Handler_ presenta una arquitectura _frontend/backend_, el _Direct Handler_ no la presenta y delega
        directamente al cliente _HTTP_ las peticiones.
      - En el _Routed Handler_, es el _backend_ el que modifica la peticion para apuntar al servidor remoto, en el _Direct Handler_
        es el cliente _HTTP_
      - En el _Routed Handler_, se crea un _frontend_ por ruta configurada y un _backend_ por _host_, en el _Direct Handler_ se crea un
        _handler_ y cliente _HTTP_ por ruta configurada.

  - El _Directed Handler_ recibio mejoras relacionados al _performance_ a lo largo de la iteración, estas son:
      - Primera mejora: Se realizan las clonaciones de cadenas de caracteres y punteros contados por referencia en un hilo de fondo.
      - Segunda mejora: Se elimina la clonacion del cliente _HTTP_.

  - Se decide implementar una segunda version del _runtime_, llamada _tuned runtime_ la cual se diferencia de la 
    implementacion previa, _simple runtime_ de la siguiente manera:
      - El _tuned runtime_ presenta una separacion entre la cantidad de hilos dedicados a aceptar peticiones y la cantidad
        dedicados a procesarla en un proporcion 50/50, al igual que el _simple runtime_.
      - El _tuned runtime_ presenta ciertas optimizaciones en cada conjunto de hilos:
        - El conjunto de hilos dedicados a aceptar peticiones esta configurado para preferir encolar nuevas _tareas_ a  
          terminar las ya existentes
        - El conjunto de hilos dedicados a procesar peticiones esta configurado para preferir terminar _tareas_ ya existentes a  
          encolar nuevas

  - El _tuned runtime_ recibio mejoras relacionados al _performance_ a lo largo de la iteración, estas son:
      - Primera mejora: Se altera la proporcion de hilos de 50/50 a 75/25, es decir, 75% de los hilos estan dedicados a procesar peticiones y el
        25% esta dedicado a aceptar peticiones de los clientes.

  - Se llevan a cabo 9 sesiones de perfilado de _grafico de llamas_ de la aplicacion, cada una siguiendo 
    una serie de cambios para mejorar el _performance_ de la aplicacion

      - Sesión 1: Este es el perfilado inicial, se ejecuta con la misma version de 
        de _CARP_ utilizada en las pruebas de _performance_ ejecutadas en el sprint anterior. 
        Se puede observar en la Figura 3.

      - Sesión 2: Con este perfilado, se establece que para obtener una mejor perspectiva
        de las llamadas realizadas, se ejecutara el escenario _Fast - Light_ de las pruebas
        de performance sobre la version de _CARP_ ejecutada. Se puede observar en la Figura 4.

      - Sesión 3: En este perfilado se hace uso del _tuned runtime_. Se puede observar en la Figura 5.

      - Sesión 4: En este perfilado se hace uso del _Direct Handler_. Se puede observar en la Figura 6.

      - Sesión 5: En este perfilado se aplica la primera mejora del _performance_ al _Direct Handler_. 
        Se puede observar en la Figura 6.

      - Sesión 6: En este perfilado se aplica la segunda mejora del _performance_ al _Direct Handler_ y la primera 
        mejora del performance al _tuned runtime_. Se puede observar en la Figura 7.

      - Sesión 7: En este perfilado se agrega trazabilidad mediante la libreria _tracing_ a _CARP_. 
        Se puede observar en la Figura 8.

      - Sesión 8: En este perfilado se remueve la trazabilidad, sin embargo, se deja el hilo encargado de procesarla, 
        de esta manera, se aprecia el impacto que tiene tanto procesar el log como el tener un hilo extra en la aplicacion. Se puede observar en la Figura 9.

      - Sesión 9: En este perfilado se implementa la trazabilidad mediante la libreria _log_, se implementa el 
        _inlining_ en ciertos metodos y se aplican ciertas optimizaciones del compilador. Se puede observar en la Figura 10.

  - Se implementan dos _benchmarks_ para comprobar que implementación del _runtime_ y _handler_ es más eficiente:

    - Se implementa un _benchmark_ que compara el performance del _Direct Handler_ y el _Route Handler_. Los resultados 
      se puede observar en la Tabla 1.

    - Se implementa un _benchmark_ que compara el performance del _tuned runtime_, el _simple runtime_ 
      y el _single runtime_. Los resultados se puede observar en la Tabla 2.

- Desarrollar CLI: servidor web, proxy reverso

  - Se implementan pruebas unitarias faltantes.
  - Se implementa un tipo de error personalizado para no depender del tipo de error de la libreria _anyhow_
  - Se refactoriza la aplicacion para no depender de las interfaces expuestas por la libreria _tower_ y se implementan
    interfaces propias a _CARP_

- Desarrollar plugin: JWT Authentication/Authorization:

  - Se implementan pruebas unitarias de la función RequestEnd con
    los siguientes escenarios:

    - Sobreescritura exitosa de la respuesta HTTP

    - Ignorar sobreescritura de la respuesta HTTP debido a
      que los datos de la respuesta con coinciden con los datos dados
      en la configuración del plugin

  - Se implementa las funciones **Configure**, **RequestStart** y **RequestEnd**
    en la infraestructura del plugin y las mismas serán expuestas
    mediantes _waPC_ para la comunicación entre _CARP_ y el plugin.
    Para el uso de _waPC_ se compilará el plugin con _tinygo_ [32]

  - Se refactoriza la función **Configure** implementada en el dominio
    del plugin debido a una incompatibilidad entre la librería
    _encoding/json_ de Go y el compilador _tinygo_. Esta
    incompatibilidad es porque la versión 0.3.3 de _tinygo_
    no soporta el uso de la librería _reflect_ de Go [33],
    _encoding/json_ usa esta última. Por lo que se sustituye la
    librería _encoding/json_ con _jsonparser_ [34], la cual si
    logra una compatibilidad con tinygo.

  - Se refactoriza la función **RequestStart** implementada en el
    dominio del plugin debido a una incompatibilidad entre la librería
    _jwt-go_ y el compilador _tinygo_, ésto debido a que _jwt-go_
    usa la librería _encoding/json_, y como ya se explicó
    anteriormente, esta última no es compatible con tinygo. Por lo
    que se realiza un _fork_ a la librería _pascaldekloe/jwt_ [35],
    ésto debido a que es una librería que cuenta con las funciones
    necesarias para nuestra lógica de negocios y es menos compleja
    que _jwt-go_, luego se pasaría a refactorizar las funciones
    a usar con el uso de _jsonparser_.

  - Se implementa las pruebas de integración de las funciones del
    plugin expuestas por _waPC_ con los siguientes escenarios:

    - Sobreescritura exitosa de la petición _HTTP_ cuando
      RequestStart es invocado

    - Envío de respuesta _HTTP_ indicando error por falta de la
      cookie cuando RequestStart es invocado

    - Sobreescritura exitosa de la respuesta _HTTP_ cuando **RequestEnd**
      es invocado

- Despliegue en ambiente de pruebas
  - Se despliega la _CARP_ en el ambiente de pruebas como un contenedor
    de docker y se llevan a cabo la ejecucion de las pruebas de _performance_

  - Se implementa la automatización de generación de gráficos de los
    resultados de las pruebas.

  - Los resultados obtenidos en la iteración 3 se pueden observar en 
    las figuras:

      - Figura 3: Peticiones por segundo de la aplicación base, seccionado
        en los diferentes tamaños de respuesta y tiempos de respuesta

      - Figura 4: Latencia de la aplicación base, seccionado
        en los diferentes tamaños de respuesta y tiempos de respuesta

      - Figura 5: Tiempo de respuesta de la aplicación base, seccionado
        en los diferentes tamaños de respuesta y tiempos de respuesta

      - Figura 6: Peticiones por segundo de la aplicación base enrutada
        por _Apache HTTPD_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 7: Latencia de la aplicación base enrutada
        por _Apache HTTPD_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 8: Tiempo de respuesta de la aplicación base enrutada
        por _Apache HTTPD_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 9: Peticiones por segundo de la aplicación base enrutada
        por _Nginx_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 10: Latencia de la aplicación base enrutada
        por _Nginx_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 11: Tiempo de respuesta de la aplicación base enrutada
        por _Nginx_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 12: Peticiones por segundo de la aplicación base enrutada
        por _HAProxy_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 13: Latencia de la aplicación base enrutada
        por _HAProxy_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 14: Tiempo de respuesta de la aplicación base enrutada
        por _HAProxy_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 15: Peticiones por segundo de la aplicación base enrutada
        por _CARP_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 16: Latencia de la aplicación base enrutada
        por _CARP_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 17: Tiempo de respuesta de la aplicación base enrutada
        por _CARP_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

    y tablas:

      - Tabla 1: Diferencia entre las peticiones por segundo de la 
        aplicacion base con cada uno de los proxies reversos, seccionados
        por rapidez de respuesta y tamaño del cuerpo de la aplicación base

      - Tabla 2: Diferencia entre la latencia de la 
        aplicacion base con cada uno de los proxies reversos, seccionados
        por rapidez de respuesta y tamaño del cuerpo de la aplicación base

      - Tabla 3: Diferencia entre los tiempos de respuesta de la 
        aplicacion base con cada uno de los proxies reversos, seccionados
        por rapidez de respuesta y tamaño del cuerpo de la aplicación base

  - Los resultados obtenidos en la iteración 4 se pueden observar en 
    las figuras:

      - Figura 18: Peticiones por segundo de la aplicación base, seccionado
        en los diferentes tamaños de respuesta y tiempos de respuesta

      - Figura 19: Latencia de la aplicación base, seccionado
        en los diferentes tamaños de respuesta y tiempos de respuesta

      - Figura 20: Tiempo de respuesta de la aplicación base, seccionado
        en los diferentes tamaños de respuesta y tiempos de respuesta

      - Figura 21: Peticiones por segundo de la aplicación base enrutada
        por _Apache HTTPD_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 22: Latencia de la aplicación base enrutada
        por _Apache HTTPD_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 23: Tiempo de respuesta de la aplicación base enrutada
        por _Apache HTTPD_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 24: Peticiones por segundo de la aplicación base enrutada
        por _Nginx_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 25: Latencia de la aplicación base enrutada
        por _Nginx_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 26: Tiempo de respuesta de la aplicación base enrutada
        por _Nginx_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 27: Peticiones por segundo de la aplicación base enrutada
        por _HAProxy_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 28: Latencia de la aplicación base enrutada
        por _HAProxy_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 29: Tiempo de respuesta de la aplicación base enrutada
        por _HAProxy_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 30: Peticiones por segundo de la aplicación base enrutada
        por _CARP_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 31: Latencia de la aplicación base enrutada
        por _CARP_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 32: Tiempo de respuesta de la aplicación base enrutada
        por _CARP_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

    y tablas:

      - Tabla 4: Diferencia entre las peticiones por segundo de la 
        aplicacion base con cada uno de los proxies reversos, seccionados
        por rapidez de respuesta y tamaño del cuerpo de la aplicación base

      - Tabla 5: Diferencia entre la latencia de la 
        aplicacion base con cada uno de los proxies reversos, seccionados
        por rapidez de respuesta y tamaño del cuerpo de la aplicación base

      - Tabla 6: Diferencia entre los tiempos de respuesta de la 
        aplicacion base con cada uno de los proxies reversos, seccionados
        por rapidez de respuesta y tamaño del cuerpo de la aplicación base