## Iteracion 2

- Desarrollar CLI Configuration inicial:
  - Se implementan dos parametros opcionales y mutuamente exclusivos, `--json` y `--yaml` para la CLI 
    que especifican el format en la que esta escrita la configuracion
  - Se implementa el parametro requerido y posicional del directorio base donde se encontraran los archivos
    _settings_, _caches_ y el directorio _routes_.
  - Se implementa la salida del comando, que muestra mediante barras de carga indefinidas el proceso
    de inicio y configuracion de la aplicacion con 5 etapas:
    - La etapa de obtencion de entradas del usuario, en las cuales se obtiene el formato y la ubicacion
      del directorio base.
    - La etapa de obtencion de fuentes de configuracion, donde se leen los contenidos de los archivos
      _settings_, _caches_ y los archivos encontrados debajo de la carpeta _routes_.
    - La etapa de deserializacion de los parametros base.
    - La etapa de deserializacion de los caches.
    - La etapa de deserializacion de las rutas.
  - Se implementa el manejo de errores de la aplicacion, mostrando en rojo el texto de la salida si alguna
    de las etapas falla en ejecutar y mostrando una descripcion corta del error y su causa
  - Se implementan las pruebas unitarias y implementa como guardia de calidad del proyecto que la 
    cobertura de dichas pruebas no debe ser inferior al 80%

- Definir contrato entre plugins:
  - Se implementa mediante wasm y la semantica de comunicacion es RPC
    y se implementara mediante wAPC.
  - Se selecciona wasmtime como runtime
  - Se implementa las estructuras de datos _Request_ y _Response_, donde
    la defincion de las mismas corresponden a las estructuras _HTTP Request_
    y _HTTP Response_
  - Se define e implementa los métodos mediante los cuales se comunicarán 
    CARP y los plugins configurados:

    - Configure: funcion que le otorga al plugin mediante un objeto json
      serializado los datos necesarios por este para llevar a cabo su 
      funcionalidad

    - RequestStart: funcion que le indica al plugin cuando se comienza
      una peticion, y se le pasa como parametro la misma peticion, 
      identificada con un Id

    - RequestEnd: funcion que le indica al plugin cuando se termina
      una peticion, y se le pasa como parametro la respuesta recibida,
      identificada con un Id

    - CacheStart: funcion que le indica al plugin cuando un cache esta 
      por ser leido y se le pasa como parametro el identificador del cache

    - CacheEnd: funcion que le indica al plugin cuando un cache fue escrito
      y se le pasa como parametro el identificador del cache y el contenido escrito

