## Iteracion 5

- Performance tunning del sprint anterior

  - Se inicia la implementación de un buffering antes de escribir/leer 
    las peticiones del socket TCP en el servidor web, de manera de reducir
    la cantidad de llamadas al kernel.

  - Se inicia la implementación de una capa de compatibilidad para ejecutar 
    _CARP_ bajo los _runtimes asíncronos_: 
      - _async-std_
      - _smol_

  - Estas decisiones se tomaron basadas en la observación de que en el 
    _gráfico de llamas_ en la sesión 9 de perfilado las operaciones predominantes son
    llamadas al kernel (_syscalls_) y especificas a _tokio_

- Desarrollar CLI: servidor web, módulo de almacén de respuestas

  - Se definen las reglas de cacheo basandose en el concepto de _memoización_ y pensando
    especificamente en las peticiones cuyo verbo se considere _idempotente_:

    - Cada respuesta almacenada en un _cache_ esta identificada segun los _parametros query_, _parametros de ruta_ y 
      _cabeceras_ configurados en el uso del cache de un ruta.
      
    - Peticiones subsecuentes que cumplan con la misma identificacion, recibiran la respuesta almacenada.

    - Un _cache_ puede depender de otros _caches_, de forma que si un _cache_ es invalidado, todos los _caches_ que dependan 
      de este son invalidados también.

    - Cuando un _cache_ es invalidado, todas las respuestas almacenadas son eliminadas, de manera que debe volver a hacerse la
      peticion al servidor remoto para volver a cachearla.

    - Una ruta puede declarar que cada vez que sea accedida, invalida una serie de _caches_

  - El almacenamiento de las respuestas se realiza mediante _archivos mapeados a memoria_, serializando las _cabeceras_, _estatus_ y _versión_
    siguiendo la especificación _bincode_ y serializando el cuerpo como bytes crudos.

- Desarrollar plugin: Routes and Cache Hits:

  - Se implementa la lógica de negocio del plugin Routes and
    Cache Hits:

    - Se implementa la estructura ResposeTime con el método Calculate,
      éste calcula el tiempo de respuesta de una petición dado el
      tiempo capturado en RequestStart y en RequestEnd.

    - Se implementa la estructura AccessTime con el método Calculate,
      éste calcula el estado actual del tiempo de acceso medio de ruta
      dada la cantidad de accesos de peticiones con una misma ruta y
      método HTTP capturadas en RequestStart, y dado el tiempo de
      inicio del plugin.

    - Se implementa la estructura ErrorTime con el método Calculate,
      éste calcula el estado actual del tiempo medio entre errores
      dada la cantidad de respuestas HTTP con errores de servidor
      capturadas en RequestEnd, y dado el tiempo de inicio del
      plugin.

    - Se implementa la estructura AverageCacheSize con el método
      Calculate, éste calcula el tamaño medio de respuesta en cache
      dada el tamaño del cuerpo del cache y la cantidad de registros
      capturados en CacheEnd

  - Se implementa las pruebas unitarias de las funciones de la
    lógica de negocio con los siguientes scenarios:

    - Obtención de tiempo respuesta: dada una petición HTTP en el
      segundo 2 y una respuesta HTTP en el segundo 5, entonces el
      tiempo de respuesta debe ser 3 segundos.

    - Obtención de tiempo medio de acceso de la ruta: dada 4
      peticiones HTTP donde 2 son peticiones GET con ruta "/app",
      entonces en el segundo 2 se debe tener un tiempo de acceso
      medio de la ruta "/app" de 1 segundo.

    - Obtención de tiempo medio entre errores: dada 5 respuestas
      HTTP donde 4 son errores de servidor, entonces en el segundo
      2 se debe de tener un tiempo medio entre errores de 2 segundos.

    - Obtención de tamaño medio de respuesta en cache: dado un tamaño
      de respuesta en cache de 100 bytes y otro de 150 bytes, entonces
      se debe tener un tamaño medio de respuesta de 125 bytes.

- Despliegue en ambiente de pruebas

  - Se actualizan los scripts para la automatización de las pruebas,
    esto con el fin de obtener un correcto registro en los logs
    una vez implementada la funcionalidad del cacheo de respuestas.

  - Los resultados obtenidos en esta iteración se pueden observar en 
    las figuras:

      - Figura 33: Peticiones por segundo de la aplicación base, seccionado
        en los diferentes tamaños de respuesta y tiempos de respuesta

      - Figura 34: Latencia de la aplicación base, seccionado
        en los diferentes tamaños de respuesta y tiempos de respuesta

      - Figura 35: Tiempo de respuesta de la aplicación base, seccionado
        en los diferentes tamaños de respuesta y tiempos de respuesta

      - Figura 36: Peticiones por segundo de la aplicación base enrutada
        por _Apache HTTPD_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 37: Latencia de la aplicación base enrutada
        por _Apache HTTPD_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 38: Tiempo de respuesta de la aplicación base enrutada
        por _Apache HTTPD_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 39: Peticiones por segundo de la aplicación base enrutada
        por _Nginx_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 40: Latencia de la aplicación base enrutada
        por _Nginx_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 41: Tiempo de respuesta de la aplicación base enrutada
        por _Nginx_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 42: Peticiones por segundo de la aplicación base enrutada
        por _HAProxy_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 43: Latencia de la aplicación base enrutada
        por _HAProxy_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 44: Tiempo de respuesta de la aplicación base enrutada
        por _HAProxy_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 45: Peticiones por segundo de la aplicación base enrutada
        por _CARP_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 46: Latencia de la aplicación base enrutada
        por _CARP_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

      - Figura 47: Tiempo de respuesta de la aplicación base enrutada
        por _CARP_, seccionado en los diferentes tamaños de respuesta 
        y tiempos de respuesta

    y tablas:

      - Tabla 7: Diferencia entre las peticiones por segundo de la 
        aplicacion base con cada uno de los proxies reversos, seccionados
        por rapidez de respuesta y tamaño del cuerpo de la aplicación base

      - Tabla 8: Diferencia entre la latencia de la 
        aplicacion base con cada uno de los proxies reversos, seccionados
        por rapidez de respuesta y tamaño del cuerpo de la aplicación base

      - Tabla 9: Diferencia entre los tiempos de respuesta de la 
        aplicacion base con cada uno de los proxies reversos, seccionados
        por rapidez de respuesta y tamaño del cuerpo de la aplicación base