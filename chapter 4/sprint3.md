## Iteracion 3

- Desarrollar CLI: servidor web y módulo de enrutamiento:

  - Se implementa el servidor web de _CARP_ usando la librería de _hyper_ de _Rust_.
  - Se implementan el _proxy reverso_ en _CARP_ mediante una arquitectura _frontend/backend_:
    - Se crea un _backend_ por cada _host_ diferente registrado en las rutas de la configuración.
    - Por cada _backend_, se crea una serie de conexiones preestablecidas con el servidor para ser
      que seran utilizadas para realizar las peticiones _HTTP_ al servidor remoto.
    - El _backend_ modifica la peticion _recibida_ por el frontend para que esta nueva petición apunte
      al _host_ del servidor remoto en vez de al _host_ de _CARP_.
    - Se crea un _frontend_ por cada _ruta_ registrada.
    - El _frontend_ se encarga de recibir las peticiones del cliente de _CARP_, valida que el método sea
      uno de los registrados (o sea el método _OPTIONS_) y reenvía la petición al _backend_ correspondiente.
  - Se establece una relación de 50/50 entre los hilos dedicados a recibir peticiones y los hilos dedicados a
    realizar las peticiones al servidor remoto

- Desarrollar plugin: JWT Authentication/Authorization:

  - Se implementa la lógica de negocio del plugin JWT de las
    siguientes funciones:

    - Configure: dado un json la función determina si el mismo es
      válido y en caso de serlo, almacena en el plugin los
      valores otorgados para ser usados en otras funcionalidades
      del plugin

    - RequestStart: dada una petición HTTP verifica la existencia
      de la cookie en petición según el nombre almacenado en la
      configuración, verifica la validez del token y sobreescribe
      los headers de la petición indicando su válida autenticacion.

    - RequestEnd: dada una respuesta HTTP sobreescribe las cookies
      de la respuesta colocando en las mismas el token suministrado
      en el cuerpo de la respuesta, luego elimina el token del
      cuerpo de la respuesta y en consiguiente sobreescribe los
      headers.

  - Se implementa las pruebas unitarias de las funciones Configure
    y RequestStart con los siguientes scenarios:

    - Configure:

      - Configuración exitosa dado un json válido

      - Configuración fallida dado un json inválido

    - RequestStart:

      - Sobreescritura exitosa de la petición HTTP

      - Envío de respuesta HTTP indicando error por falta de la
        cookie

      - Envío de respuesta HTTP indicando error por token inválido
        por la firma

      - Envío de respuesta HTTP indicando error por token inválido
        debido a la fecha de expiración

      - Envío de respuesta HTTP indicando error debido a una falla
        en la estructura del token

- Despliegue en ambiente de pruebas
  - Se despliega la _CARP_ en el ambiente de pruebas como un contenedor
    de docker y se llevan a cabo la ejecucion de las pruebas de _performance_

  - El análisis y visualización de los resultados de las pruebas tomó
    más tiempo de los esperado debido a una falta de automatización
    en la generación de los gráficos, por lo que esta tarea será
    continuada en la iteración 4.
