## Iteracion 1

- Investigación y selección de herramientas para perfilado/performance testing:

  - Se selecciona _k6_ como herramienta de performance testing.
  - Se selecciona _flamegraph-rs_ como herramienta de perfilado de codigo.

- Establecimiento del ambiente de pruebas y despliegue de pruebas:

  - Considerando que existen factores externos que pueden afectar a la medición
    de las métricas (velocidad del acceso al disco, tamaño de la banda ancha),
    se considera buscar una hardware que reduzca lo más posible la influencia
    de estos factores. A su vez, dicho hardware debe ser capaz de soportar
    multiprocesamiento, ya que los servidores web dependen de esta característica
    para poder manejar de forma eficiente grandes cantidades de peticiones
    concurrentes, por lo que se selecciona:

    - Para almacenamiento: Un disco duro SSD M.2 de 128GB

    - Para el procesador: Un Intel i3 de 11va generación con 2 núcleos y 4 hilos.

    - Para la memoria RAM: 4GB de almacenamiento DDR4

    - Para la interfaz red: Un puerto Ethernet RJ45 de hasta 1000MBits/s.

  - Se debe instalar un sistema operativo lo necesariamente vacío para la
    reducción de factores que pueda afectar a la medición de métricas. En
    consiguiente se selecciona Alpine 3.17.0

  - Se considera crear imágenes de Docker que sirva 4 servicios web donde se
    devuelva un “Hello World”. Uno de ellos está sirviendo en NGINX, otro en Apache
    HTTPD y otro en HAProxy como proxies reversos enrutando al primer servicio web,
    para así tener los cuatros posibles escenarios en el ambiente de pruebas. Asimismo
    cada scenario cuenta:

    - 3 posibles tiempos de respuesta: 200 milisegundos, 600 milisegundos y 1000
      milisegundos, correspondientes a las categorías de velocidad de un servicio web

    - 3 posibles tamanos de respuesta: 10kb, 10mb y 100 mb, correspondientes a las
      categorías de peso de un servicio web

  - Se establece el plan de las pruebas de _performace_ con los siguientes parametros:
    - La prueba dura un total de 60 segundos.
    - Se emiten 100 peticiones concurrentes cada 6 segundos transcurridos
    - Esto se traduce en, aproximadamente, 1000 peticiones por minuto, una cifra que se
      considera representativa de una carga de trabajo _pesada_, considerando que en el ambiente
      de pruebas solo puede manejar 4 procesos en paralelo, esto significa que cada sujeto
      de prueba debera manejar aproximadamente 4 peticiones de manera concurrente, por lo tanto,
      los resultados obtenidos dependeran de que tan _eficiente_ sea cada sujeto de pruebas al momento
      de manejar las peticiones concurrentes y por lo tanto se logra el objetivo de medir el nivel
      de _performance_ de cada sujeto de pruebas.

