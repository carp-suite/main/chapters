## Solución Propuesta

Para esta solución, se plantea el desarrollo de una _command line interface_ (CLI)
que sea el método de acceso principal a la aplicación, permitiendo configurar tanto
las rutas a escuchar y redirigir como los distintos _cachés_ a manejar; para
posteriormente ejecutar la aplicación, levantando así un servidor web para llevar a
cabo las redirecciones y el mantenimiento de _caché_ configurado.

Por otra parte, para demostrar la extensibilidad que ofrece la aplicación, se propone
el desarrollo de dos plugins que ejemplifican la implementación de diferentes
_crosscutting concerns_ a lo largo de un sistema desacoplado, como lo son la seguridad
y recolección de métricas; y de un _plugin_ que muestre cómo se puede sobreescribir
el comportamiento original de la aplicación, utilizando la base de datos en memoria
**Redis** cómo método de _cacheo_.

De esta forma, se propone desarrollar una CLI bajo el nombre _CARP_ que cuente con
las siguientes características:

- **Configuración inicial**: A partir de un archivo creado indicando la configuración
  de la aplicación y el directorio base donde se ejecute, la aplicación deberá de:

  - **Configurar parámetros base**: Se establecerán los parámetros base de la aplicación,
    por ejemplo, el puerto TCP de escucha, la versión del protocolo HTTP a utilizar,
    si utilizar TLS, entre otras.

  - **Registrar cachés**: Se registrará el nombre de _caches_, los parámetros de una
    petición bajo los cuales una respuesta de un servicio es _cacheada_, si el _caché_
    invalida otros _caché_ y otros parámetros que sigan la referencia de _cacheo_
    HTTP [12].

  - **Registrar rutas**: Se registrará las URL que serán enrutadas, así como el
    servicio web al que se deberá enviar las peticiones que estén dirigidas a la URL
    correspondiente.

  - **Registrar reglas de cacheo**: Se registran en conjunto a las rutas, y definen
    como un _caché_ se ve afectado después de que una petición haya ocurrido en una URL
    específica, por ejemplo, si resulta invalidado o si debe almacenarse una respuesta
    en el.

  <!-- - **Registrar plugins**: Se establecerá una conexión gRPC persistente con los
    componentes de extensión (plugin) a partir de ciertos parámetros, como la URL del
    servicio, su nombre, el uso de TLS y configuración específica al plugin. -->

  - **Registrar plugins**: Se registran los datos mendiente los cuales se invocarán
    los métodos del plugin en cuestión, como la URL del servicio, su nombre, el uso
    de TLS y configuración específica al plugin.

- **Servidor web**: A partir de la configuración provista, la aplicación levantará un
  servidor web que fungirá de:

  - **Proxy reverso**: La aplicación enrutará las peticiones, cuando corresponda, al
    servicio web correspondiente.

  - **Caché**: La aplicación _cacheará_ las respuestas de los servicios web a los que
    sirva de _proxy reverso_, respetando las reglas de _cacheo_ definidas en la
    configuración.

  - **Orquestador de plugins**: A lo largo del tiempo de vida del servidor web, el mismo
    se encargará de comunicar a los plugins sobre la ocurrencia de los siguientes
    eventos:

    - **Inicio del caché**: Notificación cuando se empieza la búsqueda en el caché, el
      plugin puede decidir sólo escuchar dicho evento o sobreescribir la respuesta del
      servidor web.

    - **Fin del caché**: Notificación cuando la búsqueda en la _caché_ finaliza y se
      encuentra una respuesta, el plugin sólo puede escuchar dicho evento.

    - **Inicio de una petición**: Notificación cuando una petición ha sido recibida de
      algún cliente, el plugin puede decidir sólo escuchar dicho evento o sobreescribir
      la respuesta del servidor.

    - **Fin de una petición**: Notificación cuando una respuesta es retornada al cliente,
      el plugin sólo puede escuchar dicho evento.

A su vez, se propone la implementación de ciertos plugins que sirvan para demostrar cómo
_CARP_ facilita la implementación, para reutilizar los _crosscutting concerns_:

- **JWT Authentication Plugin**: El plugin se encargará de, por petición, adjuntar el
  header _Authorization_ bajo el esquema _Bearer Token_ con el correspondiente
  token JWT (obtenido previamente de una respuesta de un servidor de autenticacion)
  almacenado en una _cookie_ que sera configurada, o retornará una respuesta 401
  si el mismo no está presente.

- **Routes and Cache Hits Plugin**: El plugin se encargará de recolectar las métricas de
  acceso a cada ruta y caché y mostrarlas en un dashboard web.

- **Redis Cache Plugin**: El plugin se encargará de sobreescribir el almacenamiento nativo
  del servidor web por una base de datos Redis.

Por otra parte, para confirmar que se ha alcanzado el objetivo de _alto rendimiento_, se
propone la aplicación de una serie de casos de prueba sobre _CARP_, que consistirá en
evaluar las métricas sobre una serie de _web apis_ con las obtenidas al evaluar las
métricas de _CARP_ enrutando a las _web apis_.

- Las _web apis_ a utilizar serán construidas por el equipo de desarrollo, como se tienen
  tres clasificaciones de aplicaciones web en cuestión de tiempos de respuesta (_eficiente,
  lento y moderado_)[24], se construirán 3 _web apis_ que respondan a cada una de estas
  clasificaciones.

- Las métricas a evaluar serán las mencionadas anteriormente: _latencia, peticiones por
  segundo, tiempo de respuesta_.

- Los escenarios a comparar serán:

  - **Web API**: La aplicación desarrollada, este escenario servirá como _control_ o _base_
    de las demás comparaciones.

  - **CARP (base)**: Consistirá en desplegar la web api siendo enrutada por _CARP_.

  - **CARP (redis)**: Consistirá en desplegar la _web api_ siendo enrutada por _CARP_ con
    la adición del plugin de _redis_.

  - **NGINX**: Consistirá en desplegar la _web api_ siendo enrutada por **NGINX**.

  - **Apache HTTPD**: Consistirá en desplegar la _web api_ siendo enrutada por
    **Apache HTTPD**.

  - **HAProxy**: Consistirá en desplegar cada _web api_ siendo enrutada por
    **HAProxy**.

- Se evaluarán las métricas según los criterios definidos anteriormente:

  - No se debe alterar la clasificación del sistema (_eficiente, lento o moderado_) al
    agregar CARP

  - La _latencia_ no debe aumentar en más de 30 milisegundos.

  - Las _peticiones por segundo_ no deben disminuir en más de 10 unidades.

- Se consideran que estos escenarios son suficientes para demostrar si _CARP_ cumple con
  los objetivos de performance porque:

  - Se está comparando las métricas seleccionadas entre el sistema base y el sistema
    con _CARP_ desplegado, observandose así el impacto que tiene este sobre el performance
    del sistema.
  - Se está comparando _CARP_ con soluciones defacto para _proxy reverso_ y _cacheo_, de
    manera que se pueda observar como compite _CARP_ contra soluciones del “mundo real”
    en términos de _performance_.

Para el desarrollo de esta solución se propone el seguimiento del cronograma _Anexo 1.
Cronograma de desarrollo del proyecto_ donde se hace uso de la metodología SCRUM, teniendo
un total de 10 sprints teniendo estos una duración de 2 semanas. Asimismo, en esta
planificación se emplea la metodología de Continuous Performance Managment [27], donde en
los primeros dos sprints se establecerán e implementarán el ambiente de pruebas, y en los
siguientes sprints se desplegará el producto en el ambiente de pruebas y se aplicarán pruebas
de _performance_ que midan si las métricas cumplen los objetivos establecidos, si no los
cumple se implementarán ajustes en la iteración siguiente para lograr dichos objetivos.
