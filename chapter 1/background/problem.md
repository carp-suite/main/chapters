## Planteamiento del Problema

La web ha modificado la manera en que la gestión de información y los negocios son
llevados a cabo y cada vez más se observa un incremento en las automatizaciones de
procesos de negocio a través del uso de las tecnologías en línea. De esta forma, por
el lado empresarial, se observa que en el año 2015 en los Estados Unidos, el 34.1% de
las empresas que cuentan con acceso a internet disponen de una página web, el
porcentaje aumenta para grandes empresas (85.7%) y PyMES (54.6%) [1]; en ese mismo
orden de ideas, el 61% empresas de más de 500 empleados esperaban contratar personal
de Tecnologías de la Información (TI) [2].

Por otra parte, a nivel de demanda, en el año 2000 se contabilizaban alrededor de 413
millones de internautas, mientras que para el año 2016 eran 3.14 mil millones [3].
Por lo que se puede afirmar que, a medida que ha aumentado el número de usuarios que
navegan por la red, las empresas han tomado medidas para adaptar sus servicios a un
formato digital y de acceso en línea.

De esta manera, mientras más empresas se han unido a la tendencia de automatizar sus
servicios y llevarlos a la web, cada vez más se observa que estas automatizaciones
requieren de reglas de negocio complejas, niveles de escalabilidad y rendimientos muy
exigentes y con poco margen de error, entre otros requerimientos funcionales o
suplementarios que requieren cada vez más de innovación por parte de los profesionales
de TI en la resolución de estos desafíos.

De forma que, varias técnicas fueron inventadas para tratar de garantizar estos
requerimientos, por ejemplo, para agilizar los tiempos de respuesta y reducir el consumo
de recursos de los servicios web, se utiliza el _cacheo_. Otra técnica también utilizada
para tratar de garantizar una alta eficiencia y escalabilidad es el _proxy reverso_, esta
técnica suele ser utilizada junto a otra conocida como _balanceo de carga_.

A su vez, una de las innovaciones más recientes es la arquitectura orientada a
microservicios. Asimismo, es una de las arquitecturas más populares en el mundo
empresarial, donde el 73% de las empresas con una cantidad de empleados entre 1000 y
2999 están utilizando esta arquitectura, el porcentaje aumenta a 84% con las que tienen
una cantidad entre 3000 y 4999 empleados y para las que tienen más de 5000 empleados, el
porcentaje es del 85% [8].

Sin embargo, la arquitectura de microservicios no es un panacea, y trae consigo varios
problemas e incógnitas: ¿cómo se garantiza la integridad de las transacciones?, ¿cómo
desplegar estos servicios? Y, entre las más importantes, se encuentra cómo implementar
las _crosscutting concerns_, ya que, como son comunes a todos los servicios, se genera el
problema de que cada servicio tiene su propia implementación del _crosscutting concern_ [10].

Por lo que, para tratar de solucionar los problemas mencionados en el párrafo anterior,
se tiende a hacer uso de una de las técnicas mencionadas anteriormente, el _proxy reverso_.
Sin embargo, algunos de estos _proxies_ dejan la responsabilidad del _cacheo_ al servicio
al que están enrutando, de manera que los servicios enrutados son los encargados de implementar
dicho _cacheo_ y de garantizar el determinismo del mismo, lo cual, termina resultando en
el desarrollo de múltiples algoritmos para resolver un mismo problema, duplicando así las
implementaciones de las reglas del sistema que garantizen el determinismo del _cacheo_
(o reglas de _cacheo_) donde finalmente no se soluciona el problema de la duplicación del
_crosscutting concern_.

De esta manera, cuando se mencionan las reglas de _cacheo_ de un sistema se refiere a
aquellos algoritmos que determinan, dado un almacen de respuestas _cacheadas_, cual de
ellas se retorna para una determinada peticion entrante, a la vez que decidir la validez de
estas respuestas _cacheadas_ segun el estado del sistema, se puede mencionar:

- Dada una peticion HTTP, el algoritmo que determina el identificador unico mediante el
  cual se almacenará la respuesta HTTP que se esta retornando. Por ejemplo, generar un
  valor _hash_ a partir de una cabecera de la peticion o de algun parámetro de la misma.
- De las respuestas HTTP _cacheadas_, el algoritmo que determina cual de ellas sigue siendo
  validas o no. Por ejemplo, el valor TTL de un cache que determina por cuanto tiempo un
  cache es valido despues de ser creado.

Por otra parte, aquellos proxies que habilitan el cacheo de
las respuestas de los servicios a los que enrutan, lo hacen de manera general,
siguiendo la especificación del _cacheo_ HTTP [11] o tomando en cuenta parámetros generales
como tamaño máximo de _cacheo_, tiempo de frescura del _caché_, entre otros que no
necesariamente tienen que respetar las reglas de _cacheo_ anteriormente mencionadas. Asimismo,
dentro de los componentes que implementan _proxy reverso_ o _cacheo_ podemos destacar los
siguientes:

- **Apache HTTPD**: Su implementación del _cacheo_ sigue la especificación HTTP, por lo que
  no es apto para ciertos servicios como se mencionó anteriormente [12] [13].
- **NGINX**: Permite la invalidación del _caché_ mediante una llamada especial del cliente
  con el método HTTP PURGE; esto puede resultar útil al momento de implementar ciertas reglas
  de _cacheo_, sin embargo, decae en mantenibilidad a medida que incrementa el número de
  servicios [14] [15].
- **IIS**: Al igual que Apache HTTPD, su implementación se ve limitada por los parámetros
  configurados en la especificación HTTP para el _cacheo_ [16] [17].
- **HAProxy**: Un _proxy reverso_, _balanceador de carga_ y _cache_ basado en
  que se diferencia del resto de las implementaciones por su avanzado sistema de _ACLs_,
  las cuales son reglas que el usuario puede definir para determinar como se comportaran
  las funcionalidades mencionadas anteriormente, por ejemplo, decidir que a servidor web
  enrutar o que _cache_ utilizar en una determinada peticion [18].

Por otro lado, cabe destacar la existencia de **Varnish**, un acelerador HTTP que habilita
tanto _proxy reverso_ como _cacheo_ [19] que permite definir reglas sobre el _cacheo_ que sean
específicas al sistema sobre el cual se está implementando. Pero, cabe destacar que Varnish
comparte una decisión de diseño con los componentes anteriormente mencionados (Apache HTTPD,
NGINX e IIS) que lo hacen poco apto para los sistemas de TI actuales, la cual consiste en que
la _extensibilidad_ se realiza mediante librerías compartidas (o componentes dedicados, en el
caso de **Internet Information Services (IIS)**), en específico los _shared objects (.so)_ de
los sistemas operativos \*nix o las _dynamic link libraries (.dll)_ para los sistemas operativos
MS-DOS. Esta solución es un gran reto para los equipos desarrolladores de estos servicios, ya
que, estas tecnologías suelen ser de muy bajo nivel (C/C++) y/o suelen ser costosas de
desarrollar y, posiblemente, de reutilizar en otro sistema distinto, debido a las diferencias
de las reglas de _cacheo_ que existen entre sistemas.

En otro orden de ideas, otro aspecto importante sobre el cuál este tipo de componentes
(_proxies reversos_ y _cachés_) deben tener especial cuidado es en su _performance_, este
requerimiento no funcional se ha definido de muchas maneras y por muchos autores, pero, para
este trabajo se tomará la definición del modelo _FURPS_ [20] el cual estipula que el
_performance_ se refiere a la _velocidad, eficiencia, uso de recursos, rendimiento y tiempo
de respuesta_ [21] del componente o sistema. De esta manera, los _proxies reversos_ y _cachés_
deben ser capaces de mantener _tiempos de respuesta_ y _latencia_ baja y una cantidad
_peticiones por segundo_ atendidas alta de manera que no disminuyan la _eficiencia_ del
sistema sobre el cuál se despliegan, en este aspecto, **NGINX** es el que más destaca [22] [23]
superando a **Apache HTTPD** hasta en un 50% de _peticiones por segundo_ y logrando entre
2000 [22] y 7000 [23] _peticiones por segundo_. Asimismo, existen estándares que determinan
qué valores de estos índices son considerados aceptables: para el _tiempo de respuesta_ se
considera _eficiente_ aquél que está por debajo de 200 milisegundos, _lento_ cualquiera que
sea mayor a 1 segundo y _moderado_ todo lo que esté en medio [24]; para la _latencia_, el 90%
de las organizaciones cuentan con 50 milisegundos o menos de _latencia_, mientras que el 60%
experimentan 20 milisegundos o menos [25]. De esta forma, se considerará como _alto
rendimiento_ a un _caché_ o _proxy reverso_ que:

- No altere qué tan _rápido_ es el sistema sobre el que se despliega. Es decir, si el
  sistema es considerado _rápido_ (200 o menos milisegundos de _tiempo de respuesta_),
  entonces al desplegar el _caché_ o _proxy reverso_ el _tiempo de respuesta_ no debe superar
  los 200 milisegundos o los superará en máximo 20 milisegundos.
- No aumente la _latencia_ del sistema por más de 30 milisegundos.
- No disminuya las _peticiones por segundo_ del sistema en más de 10 peticiones por segundo.

En resúmen, se considera que los componentes actuales destinados a implementar las técnicas
de _cacheo_ y _proxy reverso_ fallan en proveer la funcionalidad de cumplir con las reglas de
_cacheo_ de los servicios web bajo una arquitectura de microservicios, de manera que se
mantiene una duplicidad de funcionalidades a lo largo de todo el sistema y el mismo se
vuelve cada vez menos mantenible en temas de _cacheo_. En segundo lugar, estos componentes,
aunque presentan extensibilidad, son muy costosos de implementar, requiriendo que el equipo
tenga un conocimiento de bajo nivel o específico al componente.

Así que, se propone la creación de una aplicación de _caché_ HTTP de alto rendimiento con
capacidades de _proxy reverso_ extensible. Esta solución le proporcionará a
los desarrolladores la capacidad de controlar de una manera más granular y acorde a sus
requerimientos las reglas de _cacheo_ de sus sistemas, a su vez, permitirá incrementar los
tiempos de respuestas medios de los servicios desplegados bajo HTTP(S), ya que los mismos
no tendrán que realizar pesadas tareas de I/O para recuperar la información que deben
retornar de una manera constante, sino que será ahora _cacheada_ por un tercero hasta que
la información sea efectivamente invalidada. Por último, esta solución, al implementar
_proxy reverso_ es adecuada para que los desarrolladores implementen los _crosscutting
concerns_ de su sistema de una manera centralizada y evitar la duplicación de estas
características en cada uno de los servicios desplegados.
