## Limitaciones

- Para el formato de los archivos JSON de configuración se utilizará JSON 2020-19,
  en conjunción con la especificación JSON Schema.

- Para la aplicación CLI se utilizará rust 1.61.

- Para el servidor web, se utilizará el framework hyper 0.14.

- Para los plugins desarrollados se utilizará Go 1.19.0
