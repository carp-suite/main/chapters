## Justificacion

La mayor parte de los componentes presentados anteriormente son tecnologías que
se encuentran bajo alguna licencia _open source_ (_Apache_, _GPL_, entre otras), y,
por tanto, son tecnologías que pueden ser utilizadas a lo largo y ancho de distintos
proyectos para ayudar a los desarrolladores a implementar múltiples técnicas que
ayuden a cumplir con los requerimientos suplementarios de los sistemas que desarrollan;
entre estos requerimientos, destacan escalabilidad y rendimiento.

El objetivo de este proyecto es complementar dichas tecnologías y proveer una manera
sencilla, eficiente y escalable de implementar el _cacheo_ a los desarrolladores y que
pueda ser reutilizable en diferentes sistemas y arquitecturas, es decir, que este mismo
componente sirva para implementar el _cacheo_ de dos sistemas diferentes, pero que
pertenezcan a una misma arquitectura, de manera que se reduce a un único componente la
responsabilidad del cacheo, a diferencia de lo que se observa actualmente, en el que cada
servicio o sistema implementa su propia lógica para cumplir con las reglas de cacheo. A
su vez, como se dijo anteriormente, el objetivo es complementar a las tecnologías ya
existentes, de manera que, por ejemplo, uno de los servicios web a los que CARP haga
proxy reverso sea un servidor NGINX, el cual haga de balanceador de carga frente a otro
conjunto de servicios.

Por último, debido a la extensibilidad que presentará gracias a los plugins, _CARP_ puede
servir no solo para implementar reglas que sean específicas a cada sistema o arquitectura, sino
también para reglas generales a todas las arquitecturas existentes, un ejemplo de eso es el JWT
**Authentication/Authorization Plugin** que será desarrollado en este proyecto, ya que el mismo
sirve para cualquier arquitectura de servicios web que realice la autenticación y autorización
mediante el estándar JWT, y cuente con una autoridad que se encargue de emitirlos.
