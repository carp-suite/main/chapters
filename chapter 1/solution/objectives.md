## Objetivo General

Desarrollar un caché HTTP de alto rendimiento extensible
con proxy reverso para arquitecturas de microservicios.

## Objetivos Específicos

1. Diseñar e implementar el módulo de configuración inicial de la aplicación

2. Diseñar e implementar el módulo de servidor web de la aplicación.

3. Diseñar e implementar el módulo de CLI de la aplicación.

4. Diseñar e implementar el plugin de autenticación/autorización JWT.

5. Diseñar e implementar el plugin de recolección de métricas de coincidencias
   de _caché_ y rutas.

6. Diseñar e implementar el plugin de backend redis para el _caché_
