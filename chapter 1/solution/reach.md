## Alcance

1. Diseñar e implementar el módulo de configuración inicial de la aplicación

   Al momento de iniciar la aplicación, se proveera un directorio que debe tener una carpeta llamada
   _routes_ y dos archivos llamados _settings_ y _caches_, la aplicación inspeccionará
   los contenidos de la carpeta _routes_ en búsqueda de otros archivos JSON o YAML.
   Donde, la ubicacion de los archivos dentro de esta carpeta indicarán las URL sobre
   las cuales se aplicará proxy reverso y contendrán la información de cada ruta.
   Una vez recopilados los archivos y sus contenidos, la aplicación procederá a:

   - **Configurar parámetros base**: Se configurarán los parámetros base del servidor web,
     entre los que se encuentra:

     - **Dirección**: Dirección IP sobre la cuál se escuchará para recibir peticiones
       (por defecto: 127.0.0.1).

     - **Número de puerto**: Puerto TCP sobre el cuál se escuchará para recibir las
       peticiones.

     - **URL base**: La URL base sobre la cual se escucharán las peticiones entrantes.

     - **Protocolo**: Versión del protocolo HTTP que será utilizado en el servidor web.

     - **Certificado (opcional)**: La ruta del certificado SSL que usará el servidor para
       trabajar bajo el protocolo HTTPS.

   - **Registrar caché**: Los caché que serán aplicados a las _rutas_ sobre las que se
     actuará como _proxy reverso_, se registrarán de manera dinámica en función de los
     siguientes parámetros:

     - **Nombre**: Una cadena de caracteres que representa el nombre de la caché.

     - **Tiempo de vida**: Un número que representa el tiempo (en milisegundos) durante el
       cual el _caché_ se considera válido y se seguirá retornando como respuesta al cliente;
       cuando dicho tiempo pase, el _caché_ será invalidado y la respuesta tendrá que ser
       solicitada de nuevo al servicio.

     - **Dependencias**: Un arreglo de cadenas de caracteres que indica los nombres de los
       otros _caches_ sobre los cuales este depende, de manera que si uno de los _caché_ en
       este arreglo es invalidado, entonces este _caché_ también será invalidado.

   - **Registrar rutas**: Las URL sobre las cuales se trabajará como _proxy reverso_ y donde
     los _caché_ actuarán, se registrarán de manera dinámica en función de los siguientes
     parámetros:

     - **Camino**: Una cadena de caracteres describiendo la URL base, los parámetros de ruta
       están incorporados en dicha URL. Dichos parámetros se tomarán en cuenta para el _cacheo_.

     - **Parámetros de consulta**: Un arreglo de cadenas de caracteres indicando los
       parámetros de consulta de la ruta que se tomarán en cuenta para el _cacheo_.

     - **Encabezados**: Un arreglo de nombres de encabezados HTTP indicando los encabezados de
       la petición que se tomarán en cuenta para el cacheo.

   - **Registrar reglas de cacheo**: Están embebidas en cada configuración de ruta, en estas
     se indicarán:

     - **Nombre del método HTTP (opcional)**: Nombre del método HTTP que accionará las reglas
       de cacheo configuradas.

     - **Nombre del caché (opcional)**: Nombre del caché que la ruta accionará.

     - **Invalidaciones (opcional)**: Arreglo de nombres de _caché_ que, cuando se reciba
       una respuesta, serán invalidados.

   - **Registrar plugins**: Los componentes que actuarán como extensión al servidor
     web cuya configuración constará de:

     - **Nombre**: El nombre bajo el cual se registra el componente.

     - **URI**: URI que identifica la ubicacion del componente.

     - **Evento “inicio de caché”**: Especifica si el componente escuchará o
       sobreescribirá la respuesta del servidor cuando una respuesta haya sido recuperada
       de un caché específico.

     - **Evento “fin de caché”**: Especifica si el componente escuchará cuando una
       respuesta esté siendo enviada al cliente desde un caché en específico.

     - **Evento “inicio de petición”**: Especifica si el componente escuchará o
       sobreescribirá la respuesta del servidor cuando una petición haya entrado en una
       ruta específica del servidor.

     - **Evento “fin de petición”**: Especifica si el componente escuchará cuando una respuesta
       esté siendo retornada al cliente en una ruta específica.

     - **Configuración específica del plugin**: Una serie de opciones que sólo el plugin es capaz
       de entender y procesar.

2. Diseñar e implementar el módulo de servidor web de la aplicación.

A partir de los objetos registrados durante la configuración inicial, se ejecutará el código
requerido para correr el servidor web, que se encargará de fungir como proxy reverso a los servicios
configurados anteriormente, así como de caché HTTP de sus respuestas, donde se generan:

- **Manejadores de ruta**: Se encargarán de escuchar las rutas configuradas, accionar los _caché_,
  si corresponde, redirigir las peticiones al servicio web correspondiente, si corresponde, y notificar
  de los eventos relacionados a las rutas a los plugins.

- **Almacenes de caché**: Se encargan de almacenar las respuestas recibidas por los servicios web, así
  como de manejar su tiempo de vida y dependencias, y de notificar de los eventos relacionados al caché
  a los plugins registrados.

- **Conexiones plugin**: Se encargan de mantener una conexión persistente con los plugins registrados,
  así como de comunicar los eventos que se hayan notificado.

3. Diseñar e implementar el módulo de CLI de la aplicación.

La aplicación presentará una serie de opciones (_switches_) de terminal mediante las cuales se dará su
inicialización, así como de informar al usuario, a través de la salida estándar del proceso, de las
operaciones realizadas (inicialización de los plugins, peticiones recibidas, errores ocurridos, entre otros
tipos de información). Esta CLI será el frontend principal de la aplicación, por lo tanto, deberá cumplir
con una usabilidad que habilite un uso sencillo por parte de los usuarios.

4. Diseñar e implementar el plugin de autenticación/autorización JWT.

El plugin sobreescribirá la petición que es realizada a los servicios web en el evento de inicio de ruta,
agregando al encabezado HTTP Authorization, bajo el esquema _Bearer Token_, el token JWT que esté relacionado
con la petición (obtenido a partir de las cookies de la peticion), a su vez, modificará la salida de una de
las rutas (la que emite el token JWT) para así agregar una cookie de sesión que indique que una petición está autenticada.

5. Diseñar e implementar el plugin de recolección de métricas de coincidencias de caché y rutas.

El plugin escuchará los eventos de inicio y fin de ruta y _caché_, y recolectará ciertos datos de acceso en
cada ruta y caché que se haya configurado, proveyendo las siguientes métricas:

- Tiempo de respuesta de la ruta.

- Tiempo de acceso medio en un segundo de la ruta y caché.

- Tiempo medio entre errores de la ruta y caché.

- Tamaño medio de las respuestas almacenadas en un caché.

Y se encargará de presentar un resumen de dichas métricas en un dashboard web.

6. Diseñar e implementar el plugin de backend redis para el _caché_.

El plugin escuchará los eventos de inicio de caché, modificando la respuesta a devolver al cliente, utilizando
como método de almacenamiento una instancia de base de datos de Redis.
