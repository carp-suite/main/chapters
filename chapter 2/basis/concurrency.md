### Concurrencia

La concurrencia ha sido descrita por muchos autores de diferentes formas, de entre estas definiciones,
se puede decir que hay concurrencia cuando:

- Al menos tareas son ejecutadas en paralelo [$8].
- Se alterna entre dichas tareas sin ejecutarlas al mismo tiempo [$8] [$9].
- Se componen tareas en ejecucion independientes entre si [$10]

Por lo que, se podria decir que la concurrencia es la ejecucion de varias tareas sin un orden definido.

El (buen) manejo de la concurrencia es un topico fundamental cuando se habla de implementar servicios web,
como se definio anteriormente, un servicio web intercambia peticiones y respuestas con un cliente, pero no
solo con *un* cliente, en un momento dado, una cantidad indefinida de clientes pueden estar realizando
peticiones a dicho servicio. En este caso, no es realista exigir a un hardware tener tantos nucleos de procesador
como clientes, por lo que la eficiencia con la que contestara a cada cliente dependera de mayor medida
de como maneje sus peticiones _concurrentemente_.
