### Arquitectura de microservicios

La arquitectura de microservicios es una arquitectura de sistemas que busca organizar a los mismos 
como una coleccion de aplicaciones [$1], tambien conocidas como "servicios". Dichos servicios 
deberian implementar un "area del negocio" del sistema a construir [$2].

Dicho de otra manera, el equipo de desarrollo debe estar mas concentrado en construir servicios que 
correspondan a necesidades del negocio que implementar servicios que lidien con detalles de infraestructura.
Muchos de esos detalles de infraestructura son delegados a servicios de terceros y estos deben estar implementados
de una manera que el desarrollador tenga el control de como enlazar este servicio de tercero con la logica de 
negocio. Por ejemplo, el _proxy reverso_ **Traefik** ofrece varios metodos para _autorizar_ a los usuarios [$3]
del sistema, varios de ellos siguiendo esquemas o protocolos preestablecidos, sin embargo, tambien ofrecen 
un _middleware_ de autenticacion llamado **ForwardAuth**, el cual delega la logica de _autorizacion_ a un servicio
remoto mediante una peticion HTTP [$4].

Por lo tanto, se entiende que las arquitecturas de microservicios se centran en organizar los sistemas
como una coleccion de dos tipos de servicios: 

- Servicios de negocio, los cuales implementan un area del negocio y son implementados principalmente por el 
  equipo desarrollador del sistema
- Servicios de infraestructura, los cuales implementan una actividad en especifico que no tiene que ver con el 
  negocio, pero terminan dependiendo de los servicios de negocio para realizar dicha actividad. Estos suelen
  ser implementados por terceros.
