### Proxy Reverso

Un _proxy reverso_, también llamado _proxy inverso_, es un servidor que se sitúa por delante 
de los servidores web de un sistema y reenvía las peticiones de los clientes a dichos 
servidores [$12]

Este tipo de servicios web no representan solo la definición de los servicios de infraestructura,
sino que ademas sirven de _punto de extensión_ para implementar características de la aplicación
no relacionadas con el negocio o funcionalidad de la misma, como por ejemplo:

- _Balanceo de carga_ [$12]
- _Protección ante ataques_ [$12]
- _Alamacenamiento en cache_ [$12]
- _Encripcion SSL_ [$12]
- _Compresion_ [$13]

Por lo tanto, al momento de trabajar en una aplicación de microservicios alojada en la web, los _proxies reversos_
son la solución _de facto_ para implementar lógicas relacionadas a la infraestructura del sistema, ya que permiten
implementar _crosscutting concerns_ o _requerimientos no funcionales_ antes y después de la ejecución de la lógica de negocio, 
e incluso pueden decidir realizar _corto circuito_ y retornar temprano una respuesta al cliente sin necesidad de delegar a los 
servicios de negocio (por ejemplo, el denegar el acceso a una característica del sistema a un usuario no autenticado).
