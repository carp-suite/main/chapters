### Servicio Web

Un servicio web es un software diseñado para soportar comunicaciones de maquina a maquina sobre una red 
y se puede interactuar con ellos mediante protocolos web como HTTP o SOAP [$5] [$6]

Llevando esta definicion al contexto de una arquitectura de microservicios, un servicio web puede ser tanto
un servicio de negocio como un servicio de infraestructura que se comunica con sus clientes siguiendo un 
contrato codificado mediante protocolos Web. Poniendolo en otras palabras, es un software que recibe 
**peticiones HTTP** y retorna **respuestas HTTP**.

La idea de comunicar soportar estas comunicaciones es de proveer al cliente una funcionalidad que no puede
realizar por su cuenta [$7] y por tanto, es este ultimo el que inicia el proceso comunicativo. Por lo que,
se puede afirmar que en el contexto de una arquitectura de microservicios, si todos los servicios de negocio
son servicios web, entonces los servicios de infraestructura contaran con las tecnologias web necesarias para
comunicarse on estos, por ejemplo, un cliente HTTP.
