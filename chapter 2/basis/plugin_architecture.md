### Arquitectura de plugins

La arquitectura de plugins es una arquitectura del software establece que una aplicacion debe estar
segmentada en dos componentes: un _kernel_ e indefinidos _plugins_ [$11].

El _kernel_ esta encargado de proveer la funcionalidad base de la aplicacion, lo que significa que la
misma debe funcionar _aunque no se haya instalado ningun plugin_. A su vez, debe mantener registro
de los _plugins_ registrados y como acceder a ellos [$11].

El _plugin_ es un modulo independiente del _kernel_, pero que cuando es registrado, provee funcionalidades
adicionales al usuario de la aplicacion o puede sobreescribir el comportamiento del _kernel_. [$11]

Cabe mencionar que, entre el _kernel_ y los _plugins_ suele existir una capa abstracta conocida como
_contrato_, la cual define y gobierna la comunicacion entre estos dos tipos de componentes y suele
ser muy complicada de cambiar una vez es definida [$11].

Dado esto ultimo, no es muy popular desarrollar servicios (de cualquier tipo) siguiendo una estricta
arquitectura de plugin, sin embargo, en el caso de los microservicios y los servicios de infraestructura
si puede llegar a existir cierta similitud en ciertas decisiones de diseño. Siguiendo con el ejemplo
de **Traefik** y el middleware **ForwardAuth** al middleware se le debe especificar solo un parametro
para entrar en funcionamiento, el cual es la URL bajo esquema HTTP en la cual se encuentra alojado el
servicio web de autenticacion. Solo con saber esto, ya se pueden observar los componentes de una arquitectura
de plugins:

- El _kernel_ es la aplicacion de Traefik.
- El _plugin_ es el servicio web de autenticacion.
- El _contrato_ es el protocolo HTTPx.

Por lo que, se podria decir que la arquitectura de plugins es una aproximacion mas para comunicar los 
servicios de infraestructura a los servicios de negocio en un sistema de microservicios, solo que hay
que colocar especial cuidado al momento de diseñar el contrato mediante el cual se comunicaran.
