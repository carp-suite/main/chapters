### CLI

_CLI_, de sus siglas en ingles, _Command Line Interface_, son interfaces de usuario que permite ejecutar programas
procesos de sistemas operativos mediante entradas de texto plano [$13] o lineas de texto simple [$14]. 

Si bien es cierto que estas interfaces datan desde el 1960 [$13], siendo este el primer método mediante el cual los usuarios
ingresaban instrucciones e interactuaban con las computadoras, en tiempos recientes se han seguido utilizando este tipo de 
interfaces, hasta el punto que se ha desarrollado una guía de diseño e implementación de _CLIs_ adaptados a tiempos modernos [$15].

Al momento de interactuar con una _CLI_, existen varios conceptos o componentes con los que se debe tener cierta familiaridad:

- Argumentos (_Args_): Son parámetros posicionales [$15] y típicamente requeridos para una _CLI_.
- Switches (_Flags_): Son parámetros con nombre [$15], típicamente opcionales, que suelen tener una forma larga o una forma corta.

Se decide implementar _CARP_ como una _CLI_ debido a dos razones:

- Las _CLI_ consumen menos recursos que una interfaz de usuario gráfica (o _GUI_) [$16]. Se debe recordar que se desea que _CARP_ provea un nivel
  performance alto al momento de fungir como _proxy reverso_ y _cache HTTP_, por lo que debe dedicar la mayor cantidad de recursos
  posibles al manejo de la concurrencia y procesamiento de peticiones web.

- Las _CLI_ son la decisión adecuada para tareas repetitivas [$16]. _CARP_ recibe principalmente dos parámetros del usuario, un formato
  mediante el cual se leerá la configuración y la ubicación de la configuración, el resto de la comunicación entre el usuario y _CARP_
  recae en el _ensayo y error_ con respecto a los errores que se pueden encontrar al momento de iniciar la aplicación, por lo que dedicar
  una _GUI_ para este proceso resultaria en una experiencia mucho menos amigable, ya que, se requeririan de mas pasos en una _GUI_ para lograr
  los mismos resultados.
