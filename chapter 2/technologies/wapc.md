### WaPC

_WaPC_ (WebAssembly Protocol for Proxies) es una especificación que
define un protocolo estándar para conectar aplicaciones de host
(escritas en cualquier lenguaje) con módulos _WebAssembly_ que se
ejecutan en un entorno seguro y aislado. _WaPC_ proporciona una forma
sencilla y eficiente de invocar funciones definidas en módulos
_WebAssembly_ y recibir sus resultados, lo que permite a los
desarrolladores crear aplicaciones distribuidas y escalables que se
ejecutan en múltiples plataformas y lenguajes de programación. [1]

Se escoge esta tecnologia para el desarrollo de CARP ya que _waPC_
reduce la complejidad de la comunicación entre diferentes lenguajes,
lo que facilita la integración de módulos en el software como lo son
los plugins.
