### Flatbuffers

_FlatBuffers_ es una biblioteca de serialización de datos de alto
rendimiento y eficiente en memoria. Fue desarrollada por Google para
su uso en videojuegos y otras aplicaciones donde la latencia y la
eficiencia son críticas. En lugar de usar un formato de serialización
tradicional como _JSON_ o _XML_, _FlatBuffers_ utiliza un formato de
datos binarios altamente optimizado que se puede leer y escribir
directamente en la memoria sin necesidad de realizar operaciones
de copia o descompresión [1]. Esto hace que _FlatBuffers_ sea una
excelente opción para aplicaciones que requieren un uso intensivo de
la memoria y una baja latencia.

Se escoge esta herramienta para este desarrollo ya que se busca el
envio eficiente de estructuras complejas entre CARP y los plugins.
Esto es posible ya que _FlatBuffers_ y _waPC_ son dos tecnologías que
se complementan para ofrecer una mayor eficiencia y portabilidad en
las aplicaciones web. Al utilizar _FlatBuffers_ junto con _waPC_, se
pueden enviar datos binarios serializados de forma eficiente entre
diferentes lenguajes de programación, lo que permite una mayor
portabilidad del código y una mayor eficiencia en la comunicación
de datos.

1. https://flatbuffers.dev/
