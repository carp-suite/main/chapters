### Golang

_Go_, también conocido como _Golang_, es un lenguaje de programación
de propósito general [1] desarrollado por Google[2]. Es un lenguaje de
programación de sintaxis simple, ligero y rápido, que se ejecuta en
la plataforma de compilación de _Go_ (_Go compiler_) y produce
código en formato de archivo objeto [2]. _Go_ es un lenguaje de
programación concurrente y de alto rendimiento, que se enfoca en
facilitar la programación de aplicaciones sencillas y seguras. [2]

A su vez, _Go_ se utiliza ampliamente en el desarrollo de
aplicaciones de red, servidores web, sistemas embebidos, herramientas
de línea de comandos, entre otros [3]. Su eficiencia y capacidad para
manejar tareas en paralelo lo hacen adecuado para el procesamiento
de datos en tiempo real y aplicaciones de alta carga.

Se escoge Golang como lenguaje de programación para el desarrollo de
los plugin por lo siguiente:

- Es uno de los lenguajes que compila a Wasm [4]

- Cuenta con una libreria estandar lo suficientemente robusta para
  abarcar un gran numero de las funciones necesarias en el desarrollo
  de los plugin. [5]

- Se tiene familiaridad con el lenguaje, lo que permite ahorrar
  tiempo en la investigacion de la tecnologia y se puede adaptar a
  un cronograma ajustado. Esto se debe a que se cuenta con
  un conocimiento previo de la sintaxis y las funcionalidades del
  lenguaje, lo que facilita el desarrollo de código y la solución de
  problemas.
