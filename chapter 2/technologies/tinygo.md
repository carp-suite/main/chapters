### Tinygo

_TinyGo_ es una versión de Go diseñada para microcontroladores y
dispositivos de baja potencia. Incluye un conjunto de bibliotecas
estándar reducidas y una herramienta de compilación que optimiza el
código para su ejecución en estos dispositivos [1]. Ademas, _TinyGo_
también puede producir código _WebAssembly_ que es de tamaño muy
compacto. Puede compilar programas para navegadores web, así como para
servidores y entornos informáticos perimetrales que admitan la
familia de interfaces WebAssembly System Interface (_WASI_). [1]

La importancia de TinyGo para este desarrollo radica en que esta
tecnologia puede ser utilizada con WebAssembly. Al utilizar waPC,
TinyGo puede ser compilado a WebAssembly para su ejecución en
dispositivos que lo soporten, lo que permite una mayor portabilidad
de código y una reducción en la complejidad del desarrollo de
aplicaciones y que estas aplicaciones pueden ser fácilmente portadas
a diferentes plataformas utilizando WebAssembly como medio de
transporte. [2]
