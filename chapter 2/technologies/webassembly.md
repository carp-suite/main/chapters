### Web Assembly

_Web Assembly_

_WebAssembly_ (abreviado _Wasm_) es un formato binario de
instrucciones para una máquina virtual, diseñado para ser ejecutado
en navegadores web y otros entornos informáticos [1]. _Wasm_ es
compatible con múltiples lenguajes de programación y proporciona
un entorno de ejecución seguro y aislado para el código, lo que
lo hace adecuado para aplicaciones web de alto rendimiento y
distribuidas.

Asimismo, _Wasm_ es independiente de la plataforma y se puede ejecutar
en diferentes sistemas operativos y arquitecturas. El formato binario
compacto de _Wasm_ también reduce el tiempo de carga y mejora el
rendimiento de las aplicaciones web.[1] [2]

Considerando que _CARP_ sera extensible mendiante plugins, se escoge
_Wasm_ como una de las maneras para la incorporacion de los plugins a
CARP; ya que esto permite que los plugins sean desarrollados en
cualquier lenguaje de programación que compile a _Wasm_ en lugar de
estar limitados al lenguaje en el que está desarrollado CARP.
