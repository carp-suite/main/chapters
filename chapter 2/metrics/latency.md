### Latencia

Al momento de hablar de _latencia_ en este proyecto, se hace referencia a la _latencia de red_, la cual se define como el tiempo
de duración para que la petición llegue al servidor [$24]. También se puede definir como el tiempo de letargo en el cual una 
petición espera ser atendida.

Se decide tomar esta métrica en cuenta para las pruebas y análisis del _performance_ de _CARP_, debido a que se considera que
mientras más tiempo se tarde una petición en llegar al servidor, más es la probabilidad de que ocurran errores _pasajeros_, por
ejemplo, que la conexión entre cliente y servidor se pierda, que exista un error de protocolo durante la comunicación entre cliente
y servidor, entre otras situaciones. A su vez, una alta _latencia_ en un servicio web representa un daño a la experiencia de usuario   
del cliente del servicio, ya que, de manera aleatoria, puede experimentar retrasos en el funcionamiento del negocio.
