## Modelo Incremental 

El modelo de desarrollo incremental es el ciclo de vida de desarrollo 
software en el cual un proyecto es descompuesto en una serie de 
incrementos, cada uno de los cuales suministra una porción de la 
funcionalidad respecto de la totalidad de los requisitos del proyecto. [31]

Se utiliza este modelo gracias a las siguientes razones:

- Favorece a la retrospección en la metodología Scrum debido a que
  el modelo incremental admite cambios en la planificación. Esto es 
  util para cuando se encuentra problemas en el desarrollo de una 
  iteracion y se necesitan solucionar en una iteracion subsiguiente

- Tiene una alta sinergia con la metodología _Software Performance
  Engineering_ ya que facilita el diseño e implementación de nuevas
  soluciones en siguientes iteraciones cuando los resultados obtenidos
  del _performance testing_ no entran dentro del rango objetivo.

