## Metodología Base Scrum

_Scrum_ es una metodología de gestión de proyectos ágiles, y se usará como
soporte el marco de trabajo _Kanban_. El proceso de esta metodología en
combinación con Kanban consiste en: [29]

Crear un tablero: consiste en la creación de un tablero donde se definan
las etapas del flujo de trabajo de tu equipo (como “sin empezar”, “en
progreso” o “en revisión”) y una columna clara para las tareas finalizadas.

1. Crear un tablero: consiste en la creación de un tablero donde se definan
   las etapas del flujo de trabajo de tu equipo (como “sin empezar”, “en
   progreso” o “en revisión”) y una columna clara para las tareas finalizadas.

2. Establecer las iteraciones y los límites para la etapa de trabajo en curso:
   se define en grupo cuántas tareas puede haber y en qué etapa en un momento
   dado, para que los integrantes no se sientan agobiados con tantas tareas pero
   aún así hacer una entrega funcional.

3. Establecer las prioridades de las tareas: se establece las prioridades de
   las tareas tomando en cuenta lo que sea más útil para el producto. Dado que el
   proceso de priorización es continuo, los miembros del equipo pueden elegir lo
   que crean que es más importante para el producto.

4. Organizar reuniones de actualización: la organización de las reuniones
   ayudan a que todo el equipo entienda con qué se trabaja.

5. Realizar retrospección: evaluar si hay inconvenientes para la exitosa
   realización de la iteración y si se requiere reajustes en la planificación.

La filosofía en la metodología Scrum bajo el marco de trabajo Kanban es la mejora
continua, esta tiene como principio la idea de que si realizamos pequeñas mejoras
de forma continua a lo largo del tiempo, estas pueden conducir a cambios importantes
a largo plazo.

Esta metodología será aplicada ya que cumple con las siguientes características que
serán de utilidad durante el desarrollo del proyecto:

- La metodología Scrum en combinación con Kanban brinda a los integrantes del equipo
  la oportunidad de tomar decisiones y priorizar los trabajos según su criterio sin la
  necesidad de tener un Scrum Master o el Product Owner.

- Las iteraciones, por lo general, se trabajan en períodos incrementales de dos
  semanas, para que los miembros del equipo se puedan centrar en las tareas de cada
  sprint específico, hasta que llegue el momento de la revisión y de reiterar.

- Es un método ágil e iterativo, se pueden realizar pequeños cambios gradualmente a
  intervalos prolongados. Considerando que las necesidades de los proyectos cambian a
  medida que pasa el tiempo es lo que hace a este marco ideal para implementar en este
  proyecto.

