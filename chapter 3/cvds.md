## Esquema del CVDS

Para el desarrollo de esta solución se propone seguir el esquema
planteado en el _Anexo 2_. Esquema del CVDS, donde los objetivos se
cubren en la siguientes iteraciones:

- Para la iteración 2:

  - Diseñar e implementar el módulo de configuración inicial de la aplicación

- Para la iteración 3 e iteración 4:

  - Diseñar e implementar el módulo de servidor web de la aplicación.

  - Diseñar e implementar el módulo de CLI de la aplicación.

  - Diseñar e implementar el plugin de autenticación/autorización JWT.

- Para la iteración 5 e iteración 6:

  - Diseñar e implementar el módulo de servidor web de la aplicación.

  - Diseñar e implementar el módulo de CLI de la aplicación.

  - Diseñar e implementar el plugin de recolección de métricas de coincidencias de caché y rutas.

- Para la iteración 7 e iteración 8:

  - Diseñar e implementar el módulo de servidor web de la aplicación.

  - Diseñar e implementar el módulo de CLI de la aplicación.

  - Diseñar e implementar el plugin de backend redis para el caché.

