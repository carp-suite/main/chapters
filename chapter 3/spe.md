## Metodología Software Performance Engineering

El _Software Performance Engineering_ es un método agil para construir sistemas
de software que satisfagan ciertos requisitos de rendimiento [30]. Para esto
se hace uso de una serie de tecnicas que permiten medir el rendimiento del 
sistema aislando la mayor cantidad de variables externas posibles, entre estas 
tecnicas, estan las ya mencionadas _perfilado de software_ y _pruebas de carga_.
Ademas de aplicar estas iterativamente a lo largo del desarrollo del proyecto.

Mediante la aplicación de esta metodología se espera identificar los
problemas en el _performance_ a medida que surjan, ya que en cada iteración
(a partir de la tercera) se ejecutarán dichas pruebas, se
recolectarán los resultados y se comparan con los rangos objetivos
seleccionados en el planteamiento del problema, si estos no son satisfactorios,
se procede a alterar la implementación del _sujeto bajo pruebas_ en la siguiente
iteración.

