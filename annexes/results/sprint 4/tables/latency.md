<table id="comparison-table-lt">
  <colgroup span="3"></colgroup>
  <colgroup span="3"></colgroup>
  <colgroup span="3"></colgroup>

  <tbody>
    <tr class="comparison-table-header">
      <td rowspan="3"></td>
      <th colspan="9" scope="colgroup" style="text-align: center">Latency</th>
    </tr>                             
    <tr>                              
      <th colspan="3" scope="colgroup" style="text-align: center">Slow</th>
      <th colspan="3" scope="colgroup" style="text-align: center">Average</th>
      <th colspan="3" scope="colgroup" style="text-align: center">Fast</th>
    </tr>
    <tr>
      <th scope="col">Heavy</th>
      <th scope="col">Medium</th>
      <th scope="col">Light</th>
      <th scope="col">Heavy</th>
      <th scope="col">Medium</th>
      <th scope="col">Light</th>
      <th scope="col">Heavy</th>
      <th scope="col">Medium</th>
      <th scope="col">Light</th>
    </tr>
  </tbody>
  <tr>
    <th scope="row">Nginx</th>
    <td>0.6004991</td>
    <td>0.0011888</td>
    <td>-0.0003215</td>
    <td>0.6519573</td>
    <td>-0.4673675</td>
    <td>-0.4734833</td>
    <td>-7.3077092</td>
    <td>-0.4728139</td>
    <td>-0.4737505</td>
  </tr>
  <tr>
    <th scope="row">Httpd</th>
    <td>1.6552227</td>
    <td>0.0023357</td>
    <td>0.0024259</td>
    <td>0.2689968</td>
    <td>0.0031233</td>
    <td>-0.0219382</td>
    <td>-4.4795995</td>
    <td>-0.0048646</td>
    <td>-0.0089163</td>
  </tr>
  <tr>
    <th scope="row">Haproxy</th>
    <td>0.2840341</td>
    <td>0.0026321</td>
    <td>0.0026660</td>
    <td>1.4552864</td>
    <td>-0.4679587</td>
    <td>-0.4749409</td>
    <td>-6.7321371</td>
    <td>-0.4726276</td>
    <td>-0.4714439</td>
  </tr>
  <tr>
    <th scope="row">Carp</th>
    <td>0.2276045</td>
    <td>0.0185768</td>
    <td>0.0132530</td>
    <td>-0.1876281</td>
    <td>-0.4551092</td>
    <td>-0.4633252</td>
    <td>-7.9958392</td>
    <td>-0.4669946</td>
    <td>-0.4705426</td>
  </tr>
</table>