<table id="comparison-table-rt">
  <colgroup span="3"></colgroup>
  <colgroup span="3"></colgroup>
  <colgroup span="3"></colgroup>

  <tbody>
    <tr class="comparison-table-header">
      <td rowspan="3"></td>
      <th colspan="9" scope="colgroup" style="text-align: center">Response Time</th>
    </tr>                             
    <tr>                              
      <th colspan="3" scope="colgroup" style="text-align: center">Slow</th>
      <th colspan="3" scope="colgroup" style="text-align: center">Average</th>
      <th colspan="3" scope="colgroup" style="text-align: center">Fast</th>
    </tr>
    <tr>
      <th scope="col">Heavy</th>
      <th scope="col">Medium</th>
      <th scope="col">Light</th>
      <th scope="col">Heavy</th>
      <th scope="col">Medium</th>
      <th scope="col">Light</th>
      <th scope="col">Heavy</th>
      <th scope="col">Medium</th>
      <th scope="col">Light</th>
    </tr>
  </tbody>
  <tr>
    <th scope="row">Nginx</th>
    <td>342.4990577</td>
    <td>0.6449333</td>
    <td>0.7329411</td>
    <td>531.6051562</td>
    <td>0.4654377</td>
    <td>0.4858443</td>
    <td>322.0699307</td>
    <td>0.5604466</td>
    <td>0.5315272</td>
  </tr>
  <tr>
    <th scope="row">Httpd</th>
    <td>-0.1111326</td>
    <td>0.6439948</td>
    <td>0.6746409</td>
    <td>0.2786242</td>
    <td>0.0968813</td>
    <td>0.0798513</td>
    <td>-0.3838716</td>
    <td>0.2937286</td>
    <td>0.2904194</td>
  </tr>
  <tr>
    <th scope="row">Haproxy</th>
    <td>0.0658968</td>
    <td>0.2831356</td>
    <td>0.3021155</td>
    <td>-0.1501809</td>
    <td>-0.1052383</td>
    <td>-0.0630338</td>
    <td>-1.2288745</td>
    <td>0.1725422</td>
    <td>0.1850339</td>
  </tr>
  <tr>
    <th scope="row">Carp</th>
    <td>0.4088194</td>
    <td>0.1945142</td>
    <td>0.2953049</td>
    <td>0.9753656</td>
    <td>0.0985574</td>
    <td>0.0940988</td>
    <td>-0.0907819</td>
    <td>0.4368141</td>
    <td>0.5512469</td>
  </tr>
</table>