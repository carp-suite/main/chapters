<table id="comparison-table-rs">
  <colgroup span="3"></colgroup>
  <colgroup span="3"></colgroup>
  <colgroup span="3"></colgroup>

  <tbody>
    <tr class="comparison-table-header">
      <td rowspan="3"></td>
      <th colspan="9" scope="colgroup" style="text-align: center">Requests per Second</th>
    </tr>
    <tr>
      <th colspan="3" scope="colgroup" style="text-align: center">Slow</th>
      <th colspan="3" scope="colgroup" style="text-align: center">Average</th>
      <th colspan="3" scope="colgroup" style="text-align: center">Fast</th>
    </tr>
    <tr>
      <th scope="col">Heavy</th>
      <th scope="col">Medium</th>
      <th scope="col">Light</th>
      <th scope="col">Heavy</th>
      <th scope="col">Medium</th>
      <th scope="col">Light</th>
      <th scope="col">Heavy</th>
      <th scope="col">Medium</th>
      <th scope="col">Light</th>
    </tr>
  </tbody>
  <tr>
    <th scope="row">Nginx</th>
    <td>-0.5960616</td>
    <td>0.0003738</td>
    <td>-0.0002524</td>
    <td>-1.0550933</td>
    <td>-0.0006070</td>
    <td>0.0000648</td>
    <td>-0.9908558</td>
    <td>-0.0001587</td>
    <td>0.0003771</td>
  </tr>
  <tr>
    <th scope="row">Httpd</th>
    <td>-0.0233021</td>
    <td>-0.0000038</td>
    <td>0.0003240</td>
    <td>-0.1769392</td>
    <td>-0.0001888</td>
    <td>-0.0000785</td>
    <td>-0.0243377</td>
    <td>-0.0003279</td>
    <td>-0.0001494</td>
  </tr>
  <tr>
    <th scope="row">Haproxy</th>
    <td>-0.0273174</td>
    <td>0.0004921</td>
    <td>0.0003378</td>
    <td>-0.0876689</td>
    <td>0.0001982</td>
    <td>0.0000713</td>
    <td>-0.0345789</td>
    <td>-0.0000458</td>
    <td>0.0000845</td>
  </tr>
  <tr>
    <th scope="row">Carp</th>
    <td>-0.0140165</td>
    <td>0.0001318</td>
    <td>0.0000634</td>
    <td>-0.0108199</td>
    <td>-0.0000291</td>
    <td>-0.0001497</td>
    <td>-0.0145534</td>
    <td>-0.0002546</td>
    <td>0.0005694</td>
  </tr>
</table>
