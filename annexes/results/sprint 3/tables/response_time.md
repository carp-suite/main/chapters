<table id="comparison-table-rt">
  <colgroup span="3"></colgroup>
  <colgroup span="3"></colgroup>
  <colgroup span="3"></colgroup>

  <tbody>
    <tr class="comparison-table-header">
      <td rowspan="3"></td>
      <th colspan="9" scope="colgroup">Response Time</th>
    </tr>
    <tr>
      <th colspan="3" scope="colgroup">Slow</th>
      <th colspan="3" scope="colgroup">Average</th>
      <th colspan="3" scope="colgroup">Fast</th>
    </tr>
    <tr>
      <th scope="col">Heavy</th>
      <th scope="col">Medium</th>
      <th scope="col">Light</th>
      <th scope="col">Heavy</th>
      <th scope="col">Medium</th>
      <th scope="col">Light</th>
      <th scope="col">Heavy</th>
      <th scope="col">Medium</th>
      <th scope="col">Light</th>
    </tr>
  </tbody>
  <tr>
    <th scope="row">Nginx</th>
    <td>5.4365293</td>
    <td>0.6097153</td>
    <td>0.6049816</td>
    <td>7.6070665</td>
    <td>0.5239057</td>
    <td>0.7224583</td>
    <td>2.3610375</td>
    <td>0.5499777</td>
    <td>0.5836579</td>
  </tr>
  <tr>
    <th scope="row">Httpd</th>
    <td>-1.2703617</td>
    <td>0.6187902</td>
    <td>0.4864304</td>
    <td>1.5600380</td>
    <td>0.0228441</td>
    <td>0.2126375</td>
    <td>-0.1431767</td>
    <td>0.3613699</td>
    <td>0.2718596</td>
  </tr>
  <tr>
    <th scope="row">Haproxy</th>
    <td>-0.0374041</td>
    <td>0.2026165</td>
    <td>0.1975445</td>
    <td>0.9679243</td>
    <td>-0.1293700</td>
    <td>0.0342996</td>
    <td>-0.5350875</td>
    <td>0.2319877</td>
    <td>0.1940599</td>
  </tr>
  <tr>
    <th scope="row">Carp</th>
    <td>0.7171830</td>
    <td>1.8047463</td>
    <td>1.8007526</td>
    <td>3.6726605</td>
    <td>1.5569140</td>
    <td>1.7701274</td>
    <td>0.2976225</td>
    <td>1.7972809</td>
    <td>1.7798085</td>
  </tr>
</table>
