<table id="comparison-table-rs">
  <colgroup span="3"></colgroup>
  <colgroup span="3"></colgroup>
  <colgroup span="3"></colgroup>

  <tbody>
    <tr class="comparison-table-header">
      <td rowspan="3"></td>
      <th colspan="9" scope="colgroup">Requests per Second</th>
    </tr>
    <tr>
      <th colspan="3" scope="colgroup">Slow</th>
      <th colspan="3" scope="colgroup">Average</th>
      <th colspan="3" scope="colgroup">Fast</th>
    </tr>
    <tr>
      <th scope="col">Heavy</th>
      <th scope="col">Medium</th>
      <th scope="col">Light</th>
      <th scope="col">Heavy</th>
      <th scope="col">Medium</th>
      <th scope="col">Light</th>
      <th scope="col">Heavy</th>
      <th scope="col">Medium</th>
      <th scope="col">Light</th>
    </tr>
  </tbody>
  <tr>
    <th scope="row">Nginx</th>
    <td>-0.0017831</td>
    <td>-0.0006978</td>
    <td>-0.0004625</td>
    <td>0.0032949</td>
    <td>0.0001915</td>
    <td>0.0001830</td>
    <td>0.0051192</td>
    <td>0.0003586</td>
    <td>0.0002630</td>
  </tr>
  <tr>
    <th scope="row">Httpd</th>
    <td>-0.0262144</td>
    <td>-0.0003135</td>
    <td>-0.0006035</td>
    <td>-0.1568284</td>
    <td>-0.0002107</td>
    <td>-0.0003949</td>
    <td>-0.0133728</td>
    <td>0.0000142</td>
    <td>-0.0000513</td>
  </tr>
  <tr>
    <th scope="row">Haproxy</th>
    <td>-0.0245332</td>
    <td>-0.0002103</td>
    <td>-0.0002228</td>
    <td>-0.2051434</td>
    <td>0.0004810</td>
    <td>0.0002193</td>
    <td>-0.0175532</td>
    <td>0.0005965</td>
    <td>0.0001157</td>
  </tr>
  <tr>
    <th scope="row">Carp</th>
    <td>-0.0204284</td>
    <td>-0.0006183</td>
    <td>-0.0019139</td>
    <td>-0.0968732</td>
    <td>-0.0005784</td>
    <td>-0.0001401</td>
    <td>-0.0204385</td>
    <td>-0.0002305</td>
    <td>-0.0001869</td>
  </tr>
</table>