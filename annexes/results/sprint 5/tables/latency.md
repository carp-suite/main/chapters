<table id="comparison-table-lt">
  <colgroup span="3"></colgroup>
  <colgroup span="3"></colgroup>
  <colgroup span="3"></colgroup>

  <tbody>
    <tr class="comparison-table-header">
      <td rowspan="3"></td>
      <th colspan="9" scope="colgroup">Latency</th>
    </tr>
    <tr>
      <th colspan="3" scope="colgroup">Slow</th>
      <th colspan="3" scope="colgroup">Average</th>
      <th colspan="3" scope="colgroup">Fast</th>
    </tr>
    <tr>
      <th scope="col">Heavy</th>
      <th scope="col">Medium</th>
      <th scope="col">Light</th>
      <th scope="col">Heavy</th>
      <th scope="col">Medium</th>
      <th scope="col">Light</th>
      <th scope="col">Heavy</th>
      <th scope="col">Medium</th>
      <th scope="col">Light</th>
    </tr>
  </tbody>
  <tr>
    <th scope="row">Nginx</th>
    <td>-7.8120482</td>
    <td>0.0035105</td>
    <td>0.0066267</td>
    <td>1.7157413</td>
    <td>-0.6787313</td>
    <td>-0.6819473</td>
    <td>7.7662645</td>
    <td>-0.6807157</td>
    <td>-10.0479607</td>
  </tr>
  <tr>
    <th scope="row">Httpd</th>
    <td>-0.9350256</td>
    <td>0.6977227</td>
    <td>0.6795173</td>
    <td>-4.1058145</td>
    <td>0.0147061</td>
    <td>0.0049387</td>
    <td>-1.4283516</td>
    <td>0.0031341</td>
    <td>-9.3262402</td>
  </tr>
  <tr>
    <th scope="row">Haproxy</th>
    <td>-3.5299419</td>
    <td>0.0028400</td>
    <td>0.0107834</td>
    <td>-1.2329225</td>
    <td>-0.6799683</td>
    <td>-0.6879054</td>
    <td>0.4539022</td>
    <td>-0.6827856</td>
    <td>-10.0452849</td>
  </tr>
  <tr>
    <th scope="row">Carp</th>
    <td>-7.3107436</td>
    <td>0.0064752</td>
    <td>0.0043006</td>
    <td>-8.2617019</td>
    <td>-0.6816513</td>
    <td>-0.6920261</td>
    <td>-6.6771897</td>
    <td>-0.6818669</td>
    <td>-10.0487042</td>
  </tr>
</table>
