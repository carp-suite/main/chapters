<table id="comparison-table-rs">
  <colgroup span="3"></colgroup>
  <colgroup span="3"></colgroup>
  <colgroup span="3"></colgroup>

  <tbody>
    <tr class="comparison-table-header">
      <td rowspan="3"></td>
      <th colspan="9" scope="colgroup">Requests per Second</th>
    </tr>
    <tr>
      <th colspan="3" scope="colgroup">Slow</th>
      <th colspan="3" scope="colgroup">Average</th>
      <th colspan="3" scope="colgroup">Fast</th>
    </tr>
    <tr>
      <th scope="col">Heavy</th>
      <th scope="col">Medium</th>
      <th scope="col">Light</th>
      <th scope="col">Heavy</th>
      <th scope="col">Medium</th>
      <th scope="col">Light</th>
      <th scope="col">Heavy</th>
      <th scope="col">Medium</th>
      <th scope="col">Light</th>
    </tr>
  </tbody>
  <tr>
    <th scope="row">Nginx</th>
    <td>5.9702765</td>
    <td>0.2577124</td>
    <td>0.2576060</td>
    <td>0.5233596</td>
    <td>0.1659658</td>
    <td>0.1656106</td>
    <td>0.2875739</td>
    <td>0.0117012</td>
    <td>5.4938630</td>
  </tr>
  <tr>
    <th scope="row">Httpd</th>
    <td>0.3230075</td>
    <td>0.2735418</td>
    <td>0.2574362</td>
    <td>0.5002780</td>
    <td>0.1655269</td>
    <td>0.1494063</td>
    <td>0.2707893</td>
    <td>0.0278080</td>
    <td>5.4778060</td>
  </tr>
  <tr>
    <th scope="row">Haproxy</th>
    <td>-0.7002209</td>
    <td>0.2740166</td>
    <td>0.2737851</td>
    <td>0.5061973</td>
    <td>0.1658344</td>
    <td>0.1657971</td>
    <td>0.2722371</td>
    <td>0.0281691</td>
    <td>5.4943541</td>
  </tr>
  <tr>
    <th scope="row">Carp</th>
    <td>0.2147083</td>
    <td>0.2577914</td>
    <td>0.2576225</td>
    <td>0.3776180</td>
    <td>0.1495971</td>
    <td>0.1657445</td>
    <td>0.1739914</td>
    <td>0.0116138</td>
    <td>5.4940901</td>
  </tr>
</table>