<table id="comparison-table-rt">
  <colgroup span="3"></colgroup>
  <colgroup span="3"></colgroup>
  <colgroup span="3"></colgroup>

  <tbody>
    <tr class="comparison-table-header">
      <td rowspan="3"></td>
      <th colspan="9" scope="colgroup">Response Time</th>
    </tr>
    <tr>
      <th colspan="3" scope="colgroup">Slow</th>
      <th colspan="3" scope="colgroup">Average</th>
      <th colspan="3" scope="colgroup">Fast</th>
    </tr>
    <tr>
      <th scope="col">Heavy</th>
      <th scope="col">Medium</th>
      <th scope="col">Light</th>
      <th scope="col">Heavy</th>
      <th scope="col">Medium</th>
      <th scope="col">Light</th>
      <th scope="col">Heavy</th>
      <th scope="col">Medium</th>
      <th scope="col">Light</th>
    </tr>
  </tbody>
  <tr>
    <th scope="row">Nginx</th>
    <td>-1093.4047154</td>
    <td>-993.4033714</td>
    <td>-991.5796504</td>
    <td>-773.4902501</td>
    <td>-601.6456402</td>
    <td>-603.6785887</td>
    <td>-142.2667659</td>
    <td>-112.3950515</td>
    <td>-138.5094151</td>
  </tr>
  <tr>
    <th scope="row">Httpd</th>
    <td>-1036.3557337</td>
    <td>-992.3264491</td>
    <td>-990.4884800</td>
    <td>-169.2986740</td>
    <td>-601.4700825</td>
    <td>-603.5747124</td>
    <td>-30.3564345</td>
    <td>-112.1832143</td>
    <td>-138.4858811</td>
  </tr>
  <tr>
    <th scope="row">Haproxy</th>
    <td>-569.5514906</td>
    <td>-986.9941874</td>
    <td>-991.6804124</td>
    <td>-168.6889857</td>
    <td>-556.5854665</td>
    <td>-603.6742420</td>
    <td>-30.6560083</td>
    <td>-111.2144233</td>
    <td>-138.5685229</td>
  </tr>
  <tr>
    <th scope="row">Carp</th>
    <td>6494.9275356</td>
    <td>-1000.8602343</td>
    <td>-999.0729125</td>
    <td>6692.5920545</td>
    <td>-604.3533339</td>
    <td>-606.3474751</td>
    <td>7154.1106600</td>
    <td>-112.0653215</td>
    <td>-137.7839976</td>
  </tr>
</table>
