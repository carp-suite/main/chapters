# References

1. P. Arellano y S. Peralta (2015), “Informe de resultados:
   Tecnologías de la información y comunicación en las empresas”,
   Ministerio de Economía Fomento y Turismo, 2015.

2. Spiceworks Ziff Davis (2022), “Scanning the Future of IT
   Careers” [En línea]. Disponible en:
   https://swzd.com/resources/state-of-it/#chapter-5

3. M. Roxer, H. Ritchie y E. Ortiz-Ospina (2015), “Internet”
   [En línea]. Disponible en: https://ourworldindata.org/internet

4. AWS, “ Información general sobre el almacenamiento en caché”
   [En línea]. Disponible en: https://aws.amazon.com/es/caching/

5. Traefik Labs, “Reverse Proxy Explained: How It Works And
   When You Need One” [En línea]. Disponible en:
   https://traefik.io/glossary/reverse-proxy/

6. F5 Inc, “What Is Load Balancing?” [En línea].
   Disponible en:
   https://www.nginx.com/resources/glossary/load-balancing/

7. C. Richardson (2021), “Pattern: Microservice Architecture”
   [En línea]. Disponible en:
   https://microservices.io/patterns/microservices.html

8. L. Sujay Vailshery (2022, Febrero 23). “ Do you utilize
   microservices within your organization?” [En línea].
   Disponible en:
   https://www.statista.com/statistics/1236823/microservices-usage-per-organizati<on-size/#:~:text=Global%20usage%20of%20microservices%20in%20organizations%202021,%20by%20organization%20size&text=In%202021,%2085%20percent%20of,employees%20state%20currently%20using%20microservices

9. D. Rajput (2017), “Spring 5 Design Patterns” [En línea].
   Disponible en:
   https://www.oreilly.com/library/view/spring-5-design/9781788299459/017d98ec-cf32-4df1-b2c1-eceb06bd4985.xhtml

10. C. Richardson (2021), “Pattern: Microservice chassis”
    [En línea]. Disponible en:
    https://microservices.io/patterns/microservice-chassis.html

11. Mozilla Foundation, “HTTP caching” [En línea]. Disponible
    en:
    https://developer.mozilla.org/es/docs/Web/HTTP/Caching#expires_o_max-age

12. Apache Software Foundation, “Caching Guide” [En línea].
    Disponible en: https://httpd.apache.org/docs/2.4/caching.html

13. Apache Software Foundation, “Guía de Proxy Inverso”
    [En línea]. Disponible en:
    https://httpd.apache.org/docs/trunk/es/howto/reverse_proxy.html

14. F5 Inc, “NGINX Reverse Proxy” [En línea]. Disponible en:
    https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/

15. F5 Inc, “NGINX Content Caching” [En línea]. Disponible en:
    https://docs.nginx.com/nginx/admin-guide/content-cache/content-caching/

16. Microsoft (2022, Abril 26), “Reverse Proxy with URL Rewrite
    v2 and Application Request Routing” [En línea]. Disponible en:
    https://docs.microsoft.com/en-us/iis/extensions/url-rewrite-module/reverse-proxy-with-url-rewrite-v2-and-application-request-routing

17. Microsoft (2022, Abril 06), “Caching <caching>”
    [En línea]. Disponible en:
    https://docs.microsoft.com/en-us/iis/configuration/system.webserver/caching/

18. Tarreau (2023, Enero 22), “What HAProxy is and isn't”
    [En línea]. Disponible en: https://docs.haproxy.org/dev/intro.html#3.1

19. Varnish Software AS, “Varnish: The beef in the sandwich”
    [En línea]. Disponible en:
    https://varnish-cache.org/docs/7.1/tutorial/introduction.html#caching-with-varnish

20. R. Grady y D. Caswell (1987), “Software metrics :
    establishing a company-wide program” [En línea]. Disponible en:
    https://archive.org/details/softwaremetricse00grad/page/159/mode/2up?q=FURPS

21. L. Chung y J. Sampaio do Prado Leite (2009), “On
    Non-Functional Requirements in Software Engineering” [En línea].
    Disponible en:
    https://www.researchgate.net/publication/215697482_On_Non-Functional_Requirements_in_Software_Engineering

22. R. Sharma (2015), “NGINX High Performance”. Packt Publishing Ltd (2015)

23. D. Kunda, S. Chihana y S. Muwanei (2017). “Web Server
    Performance of Apache and Nginx: A Systematic Literature Review”
    [En linea]. Disponible en

24. R. Viscomi (2019). “ Part II Chapter 7. Performance” [En línea].
    Disponible en:  
    https://almanac.httparchive.org/en/2019/performance#time-to-first-byte-ttfb

25. A. Fael (2020). “Is Your API Real Time? Test Its Latency with
    the rtapi Tool from NGINX” [En línea]. Disponible en:
    https://www.nginx.com/blog/api-real-time-test-latency-responsiveness-nginx-rtapi-tool/#:~:text=According%20to%20the%20IDC%20report,latency%20of%2020ms%20or%20less.

26. T. Fellmann y M. Kavakli (2007). “A command line interface versus
    a graphical user interface in coding VR systems” [En línea].
    Disponible en:
    https://www.researchgate.net/publication/234818436_A_command_line_interface_versus_a_graphical_user_interface_in_coding_VR_systems

27. Dynatrace (2022). “Agile Principles for Performance Evaluation.
    Chapter: Performance Engineering” [En línea]. Disponible en:
    https://www.dynatrace.com/resources/ebooks/javabook/adopting-agile-principles/

28. https://www.scrum.org/resources/what-is-scrum
29. https://www.autentia.com/wp-content/uploads/2018/10/Mazo-KANBAN.pdf
30. https://asana.com/es/resources/scrumban
31. https://asana.com/es/resources/continuous-improvement
32. https://es.scribd.com/document/519644039/Modelo-Incremental-Asistemas#
33. https://github.com/tinygo-org/tinygo
34. https://github.com/tinygo-org/tinygo/issues/447#issuecomment-929857248
35. https://github.com/buger/jsonparser
36. https://github.com/pascaldekloe/jwt

Chapter2/Tecnologías/Web Assembly:

1.  https://webassembly.org/
2.  WebAssembly. (2021). MDN Web Docs. https://developer.mozilla.org/en-US/docs/WebAssembly

Chapter2/Tecnologías/Wapc:

1. Especificación WaPC: https://wapc.io/

Chapter2/Tecnologías/Golang:

1. https://go.dev/ref/spec
2. https://go.dev
3. https://go.dev/solutions/use-cases
4. https://github.com/golang/go/wiki/WebAssembly
5. https://pkg.go.dev/

Chapter2/Tecnologías/Tinygo:

1. https://tinygo.org/
2. https://github.com/wapc/wapc-guest-tinygo
